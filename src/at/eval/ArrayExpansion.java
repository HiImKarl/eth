package at.eval;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import at.analysis.Method;
import at.analysis.ProgramPath;
import at.ir.ArrayRefVar;
import at.ir.AssignStmt;
import at.ir.ComparisonExpr;
import at.ir.ComparisonExpr.cOp;
import at.ir.Expr;
import at.ir.IntConst;
import at.ir.Stmt;
import at.ir.Variable;
import at.util.Containers;
import at.util.Pair;

public class ArrayExpansion {
	// Expands arrays when given non-constant indices
	// I.e. a[x], where a is an array and x is an integer variable,
	// will be unrolled into N different paths, where N is the size 
	// of the array, each path gaining a constraint that x is equal to
	// one of the possible valid array indices.
	private static ArrayExpansion instance = new ArrayExpansion();
	private ArrayExpansion() {}
	public static ArrayExpansion v() { return instance; }
	
	private ProgramPath currPath;
	
	// Flag to determine when expansion should terminate
	boolean continueExpansion; 
	
	public void expandArrays(List<Method> methods) {
		do {
			this.continueExpansion = false;
			for (Method method : methods)
				method.setExpandedPaths(expandArraysHelper(method.getExpandedPaths()));
		} while(this.continueExpansion);
	}

	// Given a list of program paths, returns a list of program paths which is 
	// the previous paths with its arrays expanded so that all array indices are
	// constant values
	private List<ProgramPath> expandArraysHelper(List<ProgramPath> possiblePaths) {
		List<ProgramPath> newPaths = new ArrayList<ProgramPath>();
		for (ProgramPath path: possiblePaths) {
			this.currPath = path;
			Set<Integer> tags = new HashSet<Integer>(path.getTags());
			List<List<ComparisonExpr>> currConditions = new ArrayList<List<ComparisonExpr>>();
			tags.addAll(path.getTags());
			List<List<Stmt>> currPaths = new ArrayList<List<Stmt>>();
			// Start with single entry path
			currPaths.add(new ArrayList<Stmt>());
			currConditions.add(Containers.cloneExpr(path.getConditions()));
			for (Stmt stmt : path.getPath()) {
				if (isExpandableArrayStmt(stmt)) {
					// Set continue flag if array is found
					this.continueExpansion = true;
					Pair<List<Stmt>, List<ComparisonExpr>>
					  expandedArray = expandArrayStmt(stmt);
					  List<List<Stmt>> tmpPaths = new ArrayList<List<Stmt>>();
					  List<List<ComparisonExpr>> tmpConditions = new ArrayList<List<ComparisonExpr>>();
					  List<Stmt> expandedPaths = expandedArray.v1;
					  List<ComparisonExpr> expandedConditions = expandedArray.v2;
					  for (int i = 0; i < currPaths.size(); ++i) {
						  for (int j = 0; j < expandedPaths.size(); ++j) {
							  List<Stmt> newPath = new ArrayList<Stmt>(Containers.cloneStmt(currPaths.get(i)));
							  List<ComparisonExpr> newConditions = new ArrayList<ComparisonExpr>(
									  Containers.cloneExpr(currConditions.get(i)));
							  newPath.add(expandedPaths.get(j));
							  newConditions.add(expandedConditions.get(j));
							  tmpPaths.add(newPath);
							  tmpConditions.add(newConditions);
						  }
					  }
					  currPaths = tmpPaths;
					  currConditions = tmpConditions;
				} else {
					for (List<Stmt> currPath : currPaths) 
						currPath.add(stmt.clone());
				}
			}
			if (currConditions.size() != currPaths.size())
				throw new RuntimeException("# conditions & # paths should be the same -- program error");
			for (int i = 0; i < currPaths.size(); ++i) 
				newPaths.add(new ProgramPath(currPaths.get(i),
						currConditions.get(i), tags, path.getReturnValue(),
						path.getMethodDependencies(),
						this.currPath.getNumMethodCalls(),
						this.currPath.getPrevNumMethodCalls(),
						this.currPath.getMethodCallCounter()
						));
		}
		return newPaths;
	}

	// Returns a list of statements and the list of comparisons that are the
	// extensions to the current path by expanding the array in the RHS 
	private Pair<List<Stmt>, List<ComparisonExpr>> expandArrayStmt(Stmt stmt) {
		if (stmt instanceof AssignStmt) {
			AssignStmt assignStmt = (AssignStmt)stmt;
			Expr rhs = assignStmt.getRHS();
			Variable lhs = assignStmt.getLHS();
			if (rhs instanceof ArrayRefVar)
				return expandArrayRefVarRHS(lhs, (ArrayRefVar)rhs);
			if (lhs instanceof ArrayRefVar) 
				return expandArrayRefVarLHS((ArrayRefVar)lhs, rhs);
		}
		throw new IllegalArgumentException(stmt.toString() + " is not supported");
	}

	// expandArrayStmt for variables on the RHS
	private Pair<List<Stmt>, List<ComparisonExpr>> expandArrayRefVarRHS(
			Variable lhs, ArrayRefVar rhs) {
		if (!(rhs.getSize() instanceof IntConst))
			throw new IllegalArgumentException( "Non-constant array size: " 
					+ rhs.toString() + " is not supported");
		List<Stmt> expandedPaths = new ArrayList<Stmt>();
		List<ComparisonExpr> expandedConditions = new ArrayList<ComparisonExpr>();
		int size = ((IntConst)rhs.getSize()).getInt();
		for (int i = 0; i < size; ++i) {
			ComparisonExpr cExpr = new ComparisonExpr(
					new IntConst(i),
					cOp.EQ,
					rhs.getIndex().clone());
			expandedConditions.add(cExpr);
			ArrayRefVar refVar = rhs.clone();
			refVar.setIndex(new IntConst(i));
			AssignStmt aStmt = new AssignStmt(lhs.clone(), refVar);
			expandedPaths.add(aStmt);
		}
		return new Pair<List<Stmt>, List<ComparisonExpr>>(expandedPaths, expandedConditions);
	}
	
	// expandArrayStmt for variables on the LHS
	private Pair<List<Stmt>, List<ComparisonExpr>> expandArrayRefVarLHS(
			ArrayRefVar lhs, Expr rhs) {
		if (!(lhs.getSize() instanceof IntConst))
			throw new IllegalArgumentException( "Non-constant array size: " 
					+ lhs.toString() + " is not supported");
		List<Stmt> expandedPaths = new ArrayList<Stmt>();
		List<ComparisonExpr> expandedConditions = new ArrayList<ComparisonExpr>();
		int size = ((IntConst)lhs.getSize()).getInt();
		for (int i = 0; i < size; ++i) {
			ComparisonExpr cExpr = new ComparisonExpr(
					new IntConst(i),
					cOp.EQ,
					lhs.getIndex().clone());
			expandedConditions.add(cExpr);
			ArrayRefVar refVar = lhs.clone();
			refVar.setIndex(new IntConst(i));
			AssignStmt aStmt = new AssignStmt(refVar, rhs.clone());
			expandedPaths.add(aStmt);
		}
		return new Pair<List<Stmt>, List<ComparisonExpr>>(expandedPaths, expandedConditions);
	}

	// Returns true if stmt is an assign stmt with a RHS that 
	// is an ArrayRefVar with a non constant Expr
	private boolean isExpandableArrayStmt(Stmt stmt) {
		if (stmt instanceof AssignStmt) {
			Expr rhs = ((AssignStmt)stmt).getRHS();
			Expr lhs = ((AssignStmt)stmt).getLHS();
			if (rhs instanceof ArrayRefVar) {
				ArrayRefVar refVar = (ArrayRefVar) rhs;
				if (refVar.getIndex() instanceof Variable)
					return true;
			}
			if (lhs instanceof ArrayRefVar) {
				ArrayRefVar refVar = (ArrayRefVar) lhs;
				if (refVar.getIndex() instanceof Variable)
					return true;
			}
		}
		return false;
	}
}
