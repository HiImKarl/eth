package at.eval;

import java.util.HashSet;
import java.util.Set;


public class Reducer {
	private Set<Integer> coveredTags;

	public Reducer() {
		this.coveredTags = new HashSet<Integer>();
	}
	
	public void insertCoverage(Set<Integer> tagsCovered) {
		coveredTags.addAll(tagsCovered);
	}
	
	public boolean isRedundentCoverage(Set<Integer> tagsCovered) {
		return coveredTags.containsAll(tagsCovered);
	}
	
	public int numUncovered(Set<Integer> tags) {
		Set<Integer> tmp = new HashSet<Integer>(tags);
		tmp.removeAll(coveredTags);
		return tmp.size();
	}

}
