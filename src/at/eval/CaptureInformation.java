package at.eval;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.analysis.Method;
import at.analysis.ProgramPath;
import at.ir.ArrayRefVar;
import at.ir.ArrayVar;
import at.ir.AssignStmt;
import at.ir.BinaryExpr;
import at.ir.CastExpr;
import at.ir.ComparisonExpr;
import at.ir.Expr;
import at.ir.FieldInstance;
import at.ir.NewArrayExpr;
import at.ir.NewObjExpr;
import at.ir.ObjectVar;
import at.ir.Stmt;
import at.ir.Variable;

public class CaptureInformation {
	private static CaptureInformation instance = new CaptureInformation();
	private CaptureInformation() {}
	public static CaptureInformation v() { return instance; }
	
	private Map<String, Expr> arraySizeMap;
	private Map<String, Integer> arrayUniqueIdMap;
	private Map<String, Integer> objectUniqueIdMap;
	
	private void setUniqueId(Expr expr) {
		if (expr instanceof ArrayVar) {
			ArrayVar arrayVar = (ArrayVar) expr;
			String lvalName = arrayVar.getBaseName();
			if (arrayUniqueIdMap.containsKey(lvalName))
				arrayVar.setUniqueId(arrayUniqueIdMap.get(lvalName));
		} else if (expr instanceof ObjectVar) {
			ObjectVar objectVar = (ObjectVar) expr;
			String lvalName = objectVar.getBaseName();
			if (objectUniqueIdMap.containsKey(lvalName))
				objectVar.setUniqueId(objectUniqueIdMap.get(lvalName));
		} else if (expr instanceof BinaryExpr) {
			BinaryExpr bExpr = (BinaryExpr) expr;
			Expr lhs = bExpr.getLHS();
			Expr rhs = bExpr.getRHS();
			setUniqueId(lhs);
			setUniqueId(rhs);
		} else if (expr instanceof CastExpr) {
			CastExpr cExpr = (CastExpr) expr;
			setUniqueId(cExpr.getExpr());
		}
	}
	
	private void setSize(Expr expr) {
		if (expr instanceof ArrayVar) {
			ArrayVar arrayVar = (ArrayVar) expr;
			String lvalName = arrayVar.getBaseName();
			if (arraySizeMap.containsKey(lvalName))
				arrayVar.setSize(arraySizeMap.get(lvalName));
		} else if (expr instanceof ArrayRefVar) {
			ArrayRefVar arrayRefVar = (ArrayRefVar) expr;
			String lvalName = arrayRefVar.getBaseName();
			if (arraySizeMap.containsKey(lvalName))
				arrayRefVar.setSize(arraySizeMap.get(lvalName));
		} else if (expr instanceof BinaryExpr) {
			BinaryExpr bExpr = (BinaryExpr) expr;
			Expr lhs = bExpr.getLHS();
			Expr rhs = bExpr.getRHS();
			setSize(lhs);
			setSize(rhs);
		} else if (expr instanceof CastExpr) {
			CastExpr cExpr = (CastExpr) expr;
			setSize(cExpr.getExpr());
		}
	}
	
	private void arrayInformationHelper(Variable lval, Expr rval) {
		if (rval instanceof NewArrayExpr) {
			if (!(lval instanceof ArrayVar)) 
				throw new IllegalArgumentException("NewArrayExpr can only be assigned to ArrayVar.");
			ArrayVar arrayLval = (ArrayVar) lval;
			NewArrayExpr newArrayExpr = (NewArrayExpr) rval;
			arraySizeMap.put(arrayLval.getBaseName(), newArrayExpr.getSize());
			arrayUniqueIdMap.put(arrayLval.getBaseName(), newArrayExpr.getUniqueId());
		} else if (rval instanceof NewObjExpr) {
			if (!(lval instanceof ObjectVar))
				throw new IllegalArgumentException("NewObjExpr can only be assigned to ObjVar.");
			ObjectVar objLval = (ObjectVar) lval;
			NewObjExpr newObjExpr = (NewObjExpr) rval;
			objectUniqueIdMap.put(objLval.getBaseName(), newObjExpr.getUniqueId());
		} else if (rval instanceof ArrayVar) { 
			ArrayVar arrayRval = (ArrayVar) rval;
			if (!(lval instanceof ArrayVar))
				throw new IllegalArgumentException("Shouldn't happen, Array not being assigned to Array");
			if (!arraySizeMap.containsKey(arrayRval.getBaseName()))
				throw new RuntimeException("Shouldn't happen, array should be assigned first");
			if (!arrayUniqueIdMap.containsKey(arrayRval.getBaseName()))
				throw new RuntimeException("Shouldn't happen, array should be assigned first");
			ArrayVar arrayLval = (ArrayVar) lval;
			Expr size = arraySizeMap.get(arrayRval.getBaseName());
			arraySizeMap.put(arrayLval.getBaseName(), size);
			int uniqueId = arrayUniqueIdMap.get(arrayRval.getBaseName());
			arrayUniqueIdMap.put(arrayLval.getBaseName(), uniqueId);
		} else if (rval instanceof ObjectVar) {
			ObjectVar objRval = (ObjectVar) rval;
			ObjectVar objLval = null;
			if (lval instanceof ObjectVar) {
				 objLval = (ObjectVar) lval;
			} else if (lval instanceof FieldInstance) {
				FieldInstance field = (FieldInstance) lval;
				Variable fieldVar = field.getField();
				if (fieldVar instanceof ObjectVar)
					objLval = (ObjectVar) fieldVar;
			}
			if (objLval == null) throw new RuntimeException();
			int uniqueId = objectUniqueIdMap.get(objRval.getBaseName());
			objectUniqueIdMap.put(objLval.getBaseName(), uniqueId);
		}
		setSize(rval);
		setUniqueId(rval);
		setSize(lval);
		setUniqueId(lval);
	}
	
	private void arrayInformationStmts(List<Stmt> stmts) {
		for (Stmt stmt : stmts) {
			if (stmt instanceof AssignStmt) {
				AssignStmt assignStmt = (AssignStmt) stmt;
				Variable lval = assignStmt.getLHS();
				Expr rval = assignStmt.getRHS();
				arrayInformationHelper(lval, rval);
			}
		}
	}

	private void arrayInformationConditions(List<ComparisonExpr> conditions) {
		for (ComparisonExpr condition : conditions) {
			Expr lval = condition.getLHS();
			Expr rval = condition.getRHS();
			setSize(lval);
			setUniqueId(lval);
			setSize(rval);
			setUniqueId(rval);
		}
	}
	
	public void arrayInformation(List<Method> methods) {
		for (Method method : methods) {
			for (ProgramPath path : method.getExpandedPaths()) {
				this.arraySizeMap = new HashMap<String, Expr>();
				this.arrayUniqueIdMap = new HashMap<String, Integer>();
				this.objectUniqueIdMap = new HashMap<String, Integer>();
				arrayInformationStmts(path.getPath());
				arrayInformationConditions(path.getConditions());
			}
		}
	}
}
