package at.eval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import at.Main;
import at.analysis.Method;
import at.analysis.ProgramPath;
import at.ir.ArgumentStmt;
import at.ir.AssignStmt;
import at.ir.BinaryExpr;
import at.ir.CastExpr;
import at.ir.ComparisonExpr;
import at.ir.Expr;
import at.ir.IdentityStmt;
import at.ir.Invoke;
import at.ir.InvokeStmt;
import at.ir.ObjectVar;
import at.ir.Parameter;
import at.ir.Stmt;
import at.ir.ThisStmt;
import at.ir.Variable;
import at.ir.VirtualInvoke;
import at.util.Containers;
import at.util.Pair;

public class MethodExpansion {
	private static MethodExpansion instance = new MethodExpansion();
	private MethodExpansion() {}
	public static MethodExpansion v() { return instance; }

	private ProgramPath currPath;
	
	private int prevNumMethodCalls;
	
	// These fields are used to track the "this" variable
	// for virtual method calls, and replace "this" with the
	// invoking object
	private ObjectVar thisVar;
	
	private boolean containsInvoke;
	private boolean continueExpansion;
	
	// Create a comparator to sort program paths in order of 
	// decreasing number of program blocks reached
	private class ProgramPathComparator implements Comparator<ProgramPath> {
		@Override
		public int compare(ProgramPath p1, ProgramPath p2) {
			if (p1.getTags().size() < p2.getTags().size()) return 1;
			if (p1.getTags().size() > p2.getTags().size()) return -1;
			return 0;
		}
	}
	
	public void expandMethods(List<Method> methods) {
		do {
			this.continueExpansion = false;
			for (Method method : methods)  {
				method.setExpandedPaths(expandMethodsHelper(method.getExpandedPaths()));
			}
		} while (continueExpansion);
		
		// Sort the paths with the largest potential coverage first to 
		// try to minimize the number of redundant test cases generated
		for (Method method : methods)
			Collections.sort(method.getExpandedPaths(), new ProgramPathComparator());
	}
	
	public List<ProgramPath> expandMethodsHelper(List<ProgramPath> paths) {
		List<ProgramPath> newPaths = new ArrayList<ProgramPath>();
		for (ProgramPath path : paths) {
			this.currPath = path;
			this.prevNumMethodCalls = this.currPath.getPrevNumMethodCalls();
			int currNumMethodCalls = this.currPath.getNumMethodCalls();
			if (path.containsInvoke() == false) {
				newPaths.add(path);
				continue;
			}
			Set<Integer> tags = new HashSet<Integer>();
			List<List<ComparisonExpr>> currConditions = new ArrayList<List<ComparisonExpr>>();
			tags.addAll(path.getTags());
			List<List<Stmt>> currPaths = new ArrayList<List<Stmt>>();
			// Start with the single entry path
			currPaths.add(new ArrayList<Stmt>());
			currConditions.add(Containers.cloneExpr(path.getConditions()));
			this.containsInvoke = false;
			for (Stmt stmt : path.getPath()) {
				if (isInvokeStmt(stmt)) {
					Pair<List<List<Stmt>>, List<List<ComparisonExpr>>> 
						expandedMethod = expandInvokeStmt(stmt);
					List<List<Stmt>> tmpPaths = new ArrayList<List<Stmt>>();
					List<List<ComparisonExpr>> tmpConditions = new ArrayList<List<ComparisonExpr>>();
					List<List<Stmt>> expandedPaths = expandedMethod.v1;
					List<List<ComparisonExpr>> expandedConditions = expandedMethod.v2;
					for (int i = 0; i < currPaths.size(); ++i) {
						for (int j = 0; j < expandedPaths.size(); ++j) {
							List<Stmt> newPath = new ArrayList<Stmt>(Containers.cloneStmt(currPaths.get(i)));
							List<ComparisonExpr> newConditions = new ArrayList<ComparisonExpr>(
									Containers.cloneExpr(currConditions.get(i)));
							newPath.addAll(expandedPaths.get(j));
							newConditions.addAll(expandedConditions.get(j));
							tmpPaths.add(newPath);
							tmpConditions.add(newConditions);
						}
					}
					currPaths = tmpPaths;
					currConditions = tmpConditions;
					this.currPath.incrNumMethodCalls();
				} else {
					for (List<Stmt> currPath : currPaths) 
						currPath.add(stmt.clone());
				}
			}
			if (currConditions.size() != currPaths.size()) 
				throw new RuntimeException("# conditions & # paths should be the same -- program error");
			for (int i = 0; i < currPaths.size(); ++i) 
				newPaths.add(new ProgramPath(currPaths.get(i), 
							currConditions.get(i), tags, path.getReturnValue(), 
							path.getMethodDependencies(),
							this.currPath.getNumMethodCalls(), 
							currNumMethodCalls,
							this.currPath.getMethodCallCounter()));
			if (containsInvoke) {
				this.continueExpansion = true;
			}
		}
		return newPaths;
	}

	public static boolean isInvokeStmt(Stmt stmt) {
		if (stmt instanceof AssignStmt) {
			Expr rhs = ((AssignStmt)stmt).getRHS();
			if (rhs instanceof Invoke) return true;
		} else if (stmt instanceof InvokeStmt) {
			return true;
		}
		return false;
	}

	private Pair<List<List<Stmt>>, List<List<ComparisonExpr>>> expandInvokeStmt(Stmt stmt) {
		if (stmt instanceof AssignStmt) {
			AssignStmt assignStmt = (AssignStmt)stmt;
			Expr rhs = assignStmt.getRHS();
			Variable lhs = assignStmt.getLHS();
			if (rhs instanceof Invoke) 
				return expandInvoke(lhs, (Invoke)rhs);
		} else if (stmt instanceof InvokeStmt) {
			InvokeStmt invokeStmt = (InvokeStmt) stmt;
			// pass null as lhs so that the return value of the invoked method
			// is discarded
			return expandInvoke(null, invokeStmt.getInvoke());
		}
		throw new IllegalArgumentException(stmt.toString() + "is not supported yet");
	}

	Pair<List<List<Stmt>>, List<List<ComparisonExpr>>> expandInvoke(Variable lhs, Invoke rhs) {
		String methodFullName = rhs.getFullName();
		Method invokedMethod = Main.methodMap.get(methodFullName);
		if (invokedMethod == null)
			throw new RuntimeException("Method does not exist: " + methodFullName);
		if (rhs instanceof VirtualInvoke) {
			this.thisVar = ((VirtualInvoke)rhs).getThisVar();
		}
			
		this.currPath.incrMethodCallCount(invokedMethod);

		List<List<Stmt>> expandedPaths = new ArrayList<List<Stmt>>();
		List<List<ComparisonExpr>> expandedConditions = new ArrayList<List<ComparisonExpr>>();
		// entry path
		List<Stmt> entryPath = new ArrayList<Stmt>();
		List<Parameter> parameters = new ArrayList<Parameter>(invokedMethod.getParameters());
		List<Expr> args = rhs.getArgs();
		for (int i = 0; i < parameters.size(); ++i) {
			ArgumentStmt argStmt = new ArgumentStmt(
				parameters.get(i).clone(),
				args.get(i).clone()
					);
			addMethodCallTag(argStmt, this.currPath.getNumMethodCalls());
			entryPath.add(argStmt);
		}
		for (ProgramPath path : invokedMethod.getPossiblePaths()) {
			int totalMethodCount = this.currPath.getMethodCallCount(invokedMethod) 
					+ path.getMethodCallCount(invokedMethod);
			if (totalMethodCount > Main.recursionLimit) {
				if (path.containsInvoke()) continue;
				expandedConditions.add(new ArrayList<ComparisonExpr>());
			} else {
				List<ComparisonExpr> conditions = Containers.cloneExpr(path.getConditions());
				for (ComparisonExpr cExpr : conditions) 
					addMethodCallTag(cExpr, this.currPath.getNumMethodCalls());
				expandedConditions.add(conditions);
			}
			List<Stmt> stmts = new ArrayList<Stmt>(entryPath);
			List<Stmt> thisStmts = new ArrayList<Stmt>();
			for (Stmt stmt : path.getPath()) {
				if (isInvokeStmt(stmt))
					this.containsInvoke = true;
				Stmt copy = stmt.clone();
				addMethodCallTag(copy, this.currPath.getNumMethodCalls());
				if (copy instanceof ThisStmt) {
					ThisStmt thisStmt = (ThisStmt) copy;
					thisStmt.getRHSBox().setExpr(thisVar.clone());
					thisStmts.add(new AssignStmt(thisStmt.getRHS(), thisStmt.getLHS()));
					stmts.add(new AssignStmt(thisStmt.getLHS(), thisStmt.getRHS()));
				} else {
					stmts.add(copy);
				}
			}
			stmts.addAll(thisStmts);
			// If invoked from an AssignStmt, assign the return value of the expanded method to the
			// lhs of the AssignStmt
			if (lhs != null) {
				AssignStmt assignRV = new AssignStmt(lhs.clone(), path.getReturnValue().clone());
				addMethodCallTag(assignRV.getRHS(), this.currPath.getNumMethodCalls());
				stmts.add(assignRV);
			}
			expandedPaths.add(stmts);
		}
		if (expandedPaths.size() != expandedConditions.size()) 
			throw new RuntimeException("This should not happen -- program error");
		this.thisVar = null;
		return new Pair<List<List<Stmt>>, List<List<ComparisonExpr>>>(
				expandedPaths, expandedConditions);
	}

	private void addMethodCallTag(Stmt stmt, int callTag) {
		if (callTag < 0) return;
		if (stmt instanceof ArgumentStmt) {
			ArgumentStmt argStmt = (ArgumentStmt) stmt;
			addMethodCallTag(argStmt.getLHS(), callTag);
			// RHS calltag belongs to previous method expansion
			addMethodCallTag(argStmt.getRHS(), this.prevNumMethodCalls);
		} else if (stmt instanceof AssignStmt) {
			AssignStmt assignStmt = (AssignStmt) stmt;
			addMethodCallTag(assignStmt.getLHS(), callTag);
			addMethodCallTag(assignStmt.getRHS(), callTag);
		} else if (stmt instanceof IdentityStmt) {
			IdentityStmt identityStmt = (IdentityStmt) stmt;
			addMethodCallTag(identityStmt.getLHS(), callTag);
			addMethodCallTag(identityStmt.getRHS(), callTag);
		} else if (stmt instanceof InvokeStmt) {
			InvokeStmt invokeStmt = (InvokeStmt) stmt;
			addMethodCallTag(invokeStmt.getInvoke(), callTag);
		} else if (stmt instanceof ThisStmt) {
			ThisStmt thisStmt = (ThisStmt) stmt;
			addMethodCallTag(thisStmt.getLHS(), callTag);
		} else {
			throw new IllegalArgumentException("Dev forgot class: " + stmt.getClass());
		}
	}
	
	private void addMethodCallTag(Expr expr, int callTag) {
		if (callTag < 0) return;
		if (expr instanceof BinaryExpr) {
			BinaryExpr binaryExpr = (BinaryExpr) expr;
			addMethodCallTag(binaryExpr.getLHS(), callTag);
			addMethodCallTag(binaryExpr.getRHS(), callTag);
		} else if (expr instanceof ComparisonExpr) {
			ComparisonExpr cExpr = (ComparisonExpr) expr;
			addMethodCallTag(cExpr.getLHS(), callTag);
			addMethodCallTag(cExpr.getRHS(), callTag);
		} else if (expr instanceof CastExpr) {
			CastExpr cExpr = (CastExpr) expr;
			addMethodCallTag(cExpr.getExpr(), callTag);
		} else if (expr instanceof Parameter) {
			Parameter param = (Parameter) expr;
			param.setCallId(callTag);
		} else if (expr instanceof Variable) {
			Variable var = (Variable) expr;
			var.setCallId(callTag);
		} else if (expr instanceof VirtualInvoke) {
			VirtualInvoke virtualInvoke = (VirtualInvoke) expr;
			virtualInvoke.getThisVar().setCallId(callTag);
		}
	}
}
