package at.eval;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.RealVar;
import org.chocosolver.solver.variables.Variable;

import at.Main;
import at.ir.BinaryExpr;
import at.ir.BoolConst;
import at.ir.CastExpr;
import at.ir.ComparisonExpr;
import at.ir.DoubleConst;
import at.ir.Expr;
import at.ir.FieldInstance;
import at.ir.IntConst;
import at.ir.NewArrayExpr;
import at.ir.NewObjExpr;
import at.ir.Parameter;
import at.ir.PrimitiveParam;
import at.ir.PrimitiveType;
import at.ir.PrimitiveVar;
import at.ir.Type.Primitive;

public class Solver {
	private static Solver instance = new Solver();
	private Solver() {}
	public static Solver v() { return instance; }

	private Map<PrimitiveParam, IntVar> intParamMap;
	private Map<PrimitiveParam, RealVar> realParamMap;
	private Map<PrimitiveParam, BoolVar> boolParamMap;
	
	private Map<at.ir.Variable, IntVar> intVarMap;
	private Map<at.ir.Variable, RealVar> realVarMap;
	private Map<at.ir.Variable, BoolVar> boolVarMap;
	
	private Map<NewArrayExpr, IntVar> arrayVarMap;
	private Map<NewObjExpr, IntVar> objVarMap;
	
	private Model model;
	
	private Variable createBinaryProxy(Variable lhs, String op, Variable rhs) {
		if (lhs == null)
			throw new RuntimeException();
		if (rhs == null)
			throw new RuntimeException();
		String name = lhs.getName() + op + rhs.getName();
		if (lhs instanceof IntVar && rhs instanceof IntVar) {
			IntVar res = model.intVar(name, Main.chocoIntMin, Main.chocoIntMax);
			model.arithm((IntVar)lhs, op, (IntVar)rhs, "=", res).post();
			return res;
		} else {
			RealVar res = model.realVar(name, Main.chocoRealMin, Main.chocoRealMax, 
					Main.chocoRealPrecision);
			String equation = "{0}" + op + "{1}={2}";
			Variable[] vars = {lhs, rhs, res};
			model.realIbexGenericConstraint(equation, vars).post();
			return res;
		}
	}

	private Variable createVar(Expr expr) {
		if (expr instanceof BinaryExpr) {
			BinaryExpr bin = (BinaryExpr)expr;
			Variable left = createVar(bin.getLHS());
			Variable right = createVar(bin.getRHS());
			String op = bin.getOpStr();
			return createBinaryProxy(left, op, right);
		} else if (expr instanceof CastExpr) {
			CastExpr castExpr = (CastExpr) expr;
			Variable var = createVar(castExpr.getExpr());
			// FIXME -- Only support primitives 
			PrimitiveType type = (PrimitiveType) var;
			switch (type.getPrimitive()) {
			case DOUBLE: {
				if (!(var instanceof IntVar)) {
					throw new IllegalArgumentException(
							"Supporting only int to double cast right now, not " + var.getClass().toString());
				}
				RealVar rVar = model.realVar("<double>" + var.getName(), Main.chocoRealMin, Main.chocoRealMax, Main.chocoRealPrecision);
				Variable[] args = {rVar, var};
				model.realIbexGenericConstraint("{0}={1}", args).post();
				return rVar;
			}
			case INTEGER: {
				if (!(var instanceof RealVar))
					throw new IllegalArgumentException(
							"Supporting only double to int cast right now");
				IntVar iVar = model.intVar("<int>" + var.getName(), Main.chocoIntMin, Main.chocoIntMax);
				Variable[] args = {iVar, var};
				model.realIbexGenericConstraint("{0}={1}", args).post();
				return iVar;
			}
			default:
				throw new IllegalArgumentException("Dev frogot cast case");
			}
		} else if (expr instanceof PrimitiveParam) {
			PrimitiveParam param = (PrimitiveParam) expr;
			// FIXME -- Only support primitives 
			PrimitiveType type = (PrimitiveType) param.getType();
			if (type.getPrimitive() == Primitive.INTEGER) {
				if (!intParamMap.containsKey(param)) {
					IntVar newVar = model.intVar(param.getName(), Main.chocoIntMin, Main.chocoIntMax);
					intParamMap.put(param, newVar);
					return newVar;
				}
				return intParamMap.get(param);
			} else if (type.getPrimitive() == Primitive.DOUBLE) {
				if (!realParamMap.containsKey(param)) {
					RealVar newVar = model.realVar(param.getName(), Main.chocoRealMin, Main.chocoRealMax, Main.chocoRealPrecision);
					realParamMap.put(param, newVar);
					return newVar;
				}
				return realParamMap.get(param);
			} else if (type.getPrimitive() == Primitive.BOOLEAN) {
				if (!boolParamMap.containsKey(param)) {
					BoolVar newVar = model.boolVar(param.getName());
					boolParamMap.put(param, newVar);
					return newVar;
				}
				return boolParamMap.get(param);
			} else {
				throw new IllegalArgumentException("Expected parameter type int or doube");
			}
		} else if (expr instanceof PrimitiveVar) {
			PrimitiveVar var = (PrimitiveVar) expr;
			// FIXME -- Only support primitives 
			PrimitiveType type = var.getType();
			if (type.getPrimitive() == Primitive.INTEGER) {
				if (!intVarMap.containsKey(var)) {
					IntVar newVar = model.intVar(var.getName(), Main.chocoIntMin, Main.chocoIntMax);
					intVarMap.put(var, newVar);
					return newVar;
				}
				return intVarMap.get(var);
			} else if (type.getPrimitive() == Primitive.DOUBLE) {
				if (!realVarMap.containsKey(var)) {
					RealVar newVar = model.realVar(var.getName(), Main.chocoRealMin,
							Main.chocoRealMax, Main.chocoRealPrecision);
					realVarMap.put(var, newVar);
					return newVar;
				}
				return realVarMap.get(var);
			} else if (type.getPrimitive() == Primitive.BOOLEAN) {
				if (!boolVarMap.containsKey(var)) {
					BoolVar newVar = model.boolVar(var.getName());
					boolVarMap.put(var, newVar);
					return newVar;
				}
				return boolVarMap.get(var);
			} else {
				throw new IllegalArgumentException("Expected variable of type bool, int or double");
			}
		} else if (expr instanceof NewArrayExpr) {
			NewArrayExpr var = (NewArrayExpr) expr;
			if (!arrayVarMap.containsKey(var)) {
				IntVar newVar = model.intVar(var.toString(), var.getUniqueId());
				arrayVarMap.put(var, newVar);
				return newVar;
			}
			return arrayVarMap.get(var);
		} else if (expr instanceof NewObjExpr) {
			NewObjExpr newExpr = (NewObjExpr) expr;
			if (!objVarMap.containsKey(newExpr)) {
				IntVar newVar = model.intVar(newExpr.toString(), newExpr.getUniqueId());
				objVarMap.put(newExpr, newVar);
			}
			return objVarMap.get(newExpr);
		} else if (expr instanceof FieldInstance) {
			FieldInstance instance = (FieldInstance) expr;
			// FIXME -- Only support primitives 
			if (!(instance.getField() instanceof PrimitiveVar)) 
				throw new RuntimeException("Type not supported yet: " + instance.getField().getClass());
			PrimitiveVar primVar = (PrimitiveVar) instance.getField();
			PrimitiveType primType = primVar.getType();
			if (primType.getPrimitive() == Primitive.INTEGER) {
				if (!intVarMap.containsKey(instance)) {
					IntVar newVar = model.intVar(instance.getName(), Main.chocoIntMin, Main.chocoIntMax);
					intVarMap.put(instance, newVar);
					return newVar;
				}
				return intVarMap.get(instance);
			} else if (primType.getPrimitive() == Primitive.DOUBLE) {
				if (!realVarMap.containsKey(instance)) {
					RealVar newVar = model.realVar(instance.getName(), Main.chocoRealMin, 
							Main.chocoRealMax, Main.chocoRealPrecision);
					realVarMap.put(instance, newVar);
					return newVar;
				}
				return realVarMap.get(instance);
			} else if (primType.getPrimitive() == Primitive.BOOLEAN) {
				if (!boolVarMap.containsKey(instance)) {
					BoolVar newVar = model.boolVar(instance.getName());
					boolVarMap.put(instance, newVar);
					return newVar;
				}
				return boolVarMap.get(instance);
			} else {
				throw new IllegalArgumentException("Expected variable of type bool, int or double");
			}
		} else if (expr instanceof IntConst) {
			IntConst iVal = (IntConst)expr;
			IntVar res = model.intVar(iVal.toString(), iVal.getInt());
			return res;
		} else if (expr instanceof DoubleConst) {
			return model.realVar(((DoubleConst)expr).getDouble());
		} else {
			throw new IllegalArgumentException("Dev forgot Node type: " + expr.getClass().getName());
		}
	}
	
	/* Choco expects "=" instead of "==" for equality */
	private String pruneEqEq(String op) {
		if (op == "==") return "=";
		return op;
	}

	private void createComparison(Variable lhs, String op, Variable rhs) {
		if (lhs instanceof IntVar && rhs instanceof IntVar) {
			model.arithm((IntVar)lhs, op, (IntVar)rhs).post();
			return;
		}
		DecimalFormat df = new DecimalFormat("#");
		df.setMaximumFractionDigits(5);
		String equation;
		Variable[] vars = {lhs, rhs};
		if (op == "!=") equation = "abs({0}-{1})>" + df.format(Main.chocoRealPrecision * 10);
		else if (op.contains("=")) equation = "{0}" + op + "{1}";
		// op is <, or > and must be constrained by precision
		else equation = "{0}-{1}" + op + df.format(Main.chocoRealPrecision * 10);
		model.realIbexGenericConstraint(equation, vars).post();
	}
	
	public Map<Parameter, Expr> solve(List<ComparisonExpr> conditions, List<Parameter> parameters) {
		this.model = new Model("PV");
		this.intParamMap = new HashMap<PrimitiveParam, IntVar>();
		this.realParamMap = new HashMap<PrimitiveParam, RealVar>();
		this.boolParamMap = new HashMap<PrimitiveParam, BoolVar>();
		this.intVarMap = new HashMap<at.ir.Variable, IntVar>();
		this.realVarMap = new HashMap<at.ir.Variable, RealVar>();
		this.boolVarMap = new HashMap<at.ir.Variable, BoolVar>();
		this.arrayVarMap = new HashMap<NewArrayExpr, IntVar>();
		this.objVarMap = new HashMap<NewObjExpr, IntVar>();
		for (Parameter param: parameters) {
			// FIXME -- Only support primitives 
			if (param instanceof PrimitiveParam) {
				PrimitiveParam primParam = (PrimitiveParam) param;
				PrimitiveType type = (PrimitiveType) primParam.getType();
				if (type.getPrimitive() == Primitive.INTEGER) {
					IntVar res = model.intVar(primParam.getName(), Main.chocoIntMin, Main.chocoIntMax);
					intParamMap.put(primParam, res);
				} else if (type.getPrimitive() == Primitive.DOUBLE) {
					RealVar res = model.realVar(primParam.getName(), Main.chocoRealMin, Main.chocoRealMax, Main.chocoRealPrecision);
					realParamMap.put(primParam, res);
				} else if (type.getPrimitive() == Primitive.BOOLEAN) {
					BoolVar res = model.boolVar(primParam.getName());
					boolParamMap.put(primParam, res);
				} else {
					throw new IllegalArgumentException("Expecting int, real, or boolean parameter type");
				}
			}
		}
		for (ComparisonExpr condition : conditions) {
			Variable left = createVar(condition.getLHS());
			Variable right = createVar(condition.getRHS());
			createComparison(left, pruneEqEq(condition.getOpStr()), right);
		}
		Solution solution = model.getSolver().findSolution();
		if (solution != null) {
			Map<Parameter, Expr> solutionSet = new HashMap<Parameter, Expr>();
			for (Parameter param : parameters) {
				// FIXME -- Only support primitives 
				if (param instanceof PrimitiveParam) {
					PrimitiveParam primParam = (PrimitiveParam) param;
					PrimitiveType type = (PrimitiveType)primParam.getType();
					if (type.getPrimitive() == Primitive.INTEGER) {
						solutionSet.put(param, new IntConst(solution.getIntVal(intParamMap.get(primParam))));
					} else if (type.getPrimitive() == Primitive.DOUBLE) {
						solutionSet.put(param, new DoubleConst(solution.getRealBounds(realParamMap.get(primParam))[0]));
					} else if (type.getPrimitive() == Primitive.BOOLEAN) {
						int val = solution.getIntVal(boolParamMap.get(primParam));
						if (val != 0 && val != 1)
							throw new RuntimeException("Something went wrong...");
						boolean b = val == 1;
						solutionSet.put(param, new BoolConst(b));
					} else {
						throw new IllegalArgumentException("Type: " + primParam.getType().toString() + " not supported");
					}
				}
			}
			return solutionSet;
		} else {
			return null;
		}
	}
}
