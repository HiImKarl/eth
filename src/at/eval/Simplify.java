package at.eval;

import java.util.*;

import at.analysis.ProgramPath;
import at.ir.*;

public class Simplify {
	private static Simplify instance = new Simplify();
	private Simplify() {}
	public static Simplify v() { return instance; }
	
	// FIXME -- recursive solution will become problematic 
	// with larger input programs
	private void simplifyConditionHelper(ExprBox<Expr> exprBox, Variable lval, Expr rval) {
		if (exprBox.getExpr() instanceof BinaryExpr) {
			BinaryExpr expr = (BinaryExpr) exprBox.getExpr();
			simplifyConditionHelper(expr.getLHSBox(), lval, rval);
			simplifyConditionHelper(expr.getRHSBox(), lval, rval);
		} else if (exprBox.getExpr() instanceof PrimitiveVar) {
			PrimitiveVar local = (PrimitiveVar)exprBox.getExpr();
			if (local.equals(lval))
				exprBox.setExpr(rval.clone());
		} else if (exprBox.getExpr() instanceof ArrayVar) {
			ArrayVar local = (ArrayVar)exprBox.getExpr();
			if (local.equals(lval))
				exprBox.setExpr(rval.clone());
		} else if (exprBox.getExpr() instanceof ObjectVar) {
			ObjectVar local = (ObjectVar)exprBox.getExpr();
			if (local.equals(lval))
				exprBox.setExpr(rval.clone());
		} else if (exprBox.getExpr() instanceof ArrayRefVar) {
			ArrayRefVar local = (ArrayRefVar) exprBox.getExpr();
			if (lval instanceof ArrayRefVar) {
				if (local.equals(lval))
					exprBox.setExpr(rval.clone());
			} else if (lval instanceof ArrayVar) {
				// If not an ArrayVar (could be NewArrayExpr), info already propagated
				if (!(rval instanceof ArrayVar)) return;
				ArrayVar arrayRVal = (ArrayVar) rval;
				if (local.getBaseName().equals(lval.getBaseName())) {
					local.setBaseName(arrayRVal);
					local.setSize(arrayRVal.getSize());
				}
			}
		} else if (exprBox.getExpr() instanceof FieldInstance) {
			FieldInstance local = (FieldInstance) exprBox.getExpr();
			if (lval instanceof FieldInstance) {
				FieldInstance instance = (FieldInstance) lval;
				if (local.equals(instance)) {
					exprBox.setExpr(rval.clone());
					return;
				}
				if (local.getFullBaseName().equals(instance.getName())) {
					if (rval instanceof ObjectVar) {
						ObjectVar ovar = (ObjectVar) rval;
						exprBox.setExpr(new FieldInstance(ovar, local.getLastField()));
					} else if (rval instanceof FieldInstance) {
						FieldInstance rvalInstance = ((FieldInstance)rval).clone();
						rvalInstance.setLastField(local.getLastField());
						exprBox.setExpr(rvalInstance);
					} else {
						throw new RuntimeException();
					}

				}
			} else if (lval instanceof ObjectVar) {
				// If not an ObjVar or FieldInstance (could be NewObjExpr), info already propagated
				if (rval instanceof FieldInstance) {
					if (!local.getFullBaseName().equals(lval.getBaseName())) return;
					FieldInstance fieldInstance = (FieldInstance) rval.clone();
					ObjectVar obj = (ObjectVar) fieldInstance.getLastField();
					FieldInstance newField = new FieldInstance(obj, local.getField());
					fieldInstance.setLastField(newField);
					exprBox.setExpr(fieldInstance);
				} else if (rval instanceof ObjectVar) {
					ObjectVar objectRVal = (ObjectVar) rval;
					if (local.getBaseName().equals(lval.getBaseName())) {
						local.setBaseName(objectRVal);
					}
				}
			}
		} else if (exprBox.getExpr() instanceof CastExpr) {
			CastExpr castExpr = (CastExpr)exprBox.getExpr();
			simplifyConditionHelper(castExpr.getExprBox(), lval, rval);
		} 
	}
	
	private void simplifyConditionHelper(ExprBox<Expr> exprBox, Parameter lval, Expr rval) {
		if (exprBox.getExpr() instanceof BinaryExpr) {
			BinaryExpr expr = (BinaryExpr) exprBox.getExpr();
			simplifyConditionHelper(expr.getLHSBox(), lval, rval);
			simplifyConditionHelper(expr.getRHSBox(), lval, rval);
		} else if (exprBox.getExpr() instanceof Parameter) {
			Parameter local = (Parameter)exprBox.getExpr();
			if (local.equals(lval))
				exprBox.setExpr(rval.clone());
		} else if (exprBox.getExpr() instanceof CastExpr) {
			CastExpr castExpr = (CastExpr)exprBox.getExpr();
			simplifyConditionHelper(castExpr.getExprBox(), lval, rval);
		} 
	}
	
	private void simplifyCondition(ComparisonExpr baseCondition, List<Stmt> stmts) {
		ListIterator<Stmt> exprRIt = stmts.listIterator(stmts.size());
		while (exprRIt.hasPrevious()) {
			Stmt stmt = exprRIt.previous();
			if (stmt instanceof AssignStmt) {
				AssignStmt assignStmt = (AssignStmt) stmt;
				Variable lval = assignStmt.getLHS();
				Expr rval = assignStmt.getRHS();
				simplifyConditionHelper(baseCondition.getLHSBox(), lval, rval);
				simplifyConditionHelper(baseCondition.getRHSBox(), lval, rval);
			} else if (stmt instanceof IdentityStmt) {
				IdentityStmt identityStmt = (IdentityStmt)stmt;
				Variable lval = identityStmt.getLHS();
				Parameter rval = identityStmt.getRHS();
				simplifyConditionHelper(baseCondition.getLHSBox(), lval, rval);
				simplifyConditionHelper(baseCondition.getRHSBox(), lval, rval);
			} else if (stmt instanceof ArgumentStmt) {
				ArgumentStmt argumentStmt = (ArgumentStmt)stmt;
				Parameter lval = argumentStmt.getLHS();
				Expr rval = argumentStmt.getRHS();
				simplifyConditionHelper(baseCondition.getLHSBox(), lval, rval);
				simplifyConditionHelper(baseCondition.getRHSBox(), lval, rval);
			} else {
				//throw new IllegalArgumentException("Unsupported type");
			}
		}
	}
	
	public void simplifyConditions(ProgramPath path) {
		List<ComparisonExpr> simplified = new ArrayList<ComparisonExpr>();
		for (ComparisonExpr condition : path.getConditions()) {
			simplifyCondition(condition, path.getPath());
			simplified.add(condition);
		}
		path.setConditions(simplified);
	}
}
