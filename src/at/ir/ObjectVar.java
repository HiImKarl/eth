package at.ir;

import soot.RefType;
import soot.jimple.internal.JimpleLocal;


public class ObjectVar extends Variable {
	private ObjectType type;

	// This field is used to resolve object reference equivalence
	int uniqueId = -1;
	
	public ObjectVar(String name, ObjectType type) {
		super(name);
		this.type = type;
	}
	
	public ObjectVar(ObjectVar var) {
		super(var);
		this.type = var.type.clone();
		this.uniqueId = var.uniqueId;
	}

	@Override
	public ObjectVar clone() {
		return new ObjectVar(this);
	}
	
	public ObjectType getType() {
		return type;
	}

	public int getUniqueId() {
		return this.uniqueId;
	}
	
	public void setUniqueId(int uniqueId) {
		this.uniqueId = uniqueId;
	}
	
	public boolean isUniqueIdSet() {
		return uniqueId != -1;
	}

	@Override
	public String getName() {
		return this.getBaseName();
	}

	@Override
	public String getFullName() {
		return this.getName();
	}
	
	@Override 
	public String toString() {
		return this.getFullName() + "(" + methodId + ")" + "|" + type.toString() + ";" + uniqueId;
	}
	
	public static ObjectVar convertFromSoot(JimpleLocal jLocal) {
		if (!(jLocal.getType() instanceof RefType)) 
			throw new IllegalArgumentException("Expecting Ref JimpleLocal");
		RefType refType = (RefType) jLocal.getType();
		return new ObjectVar(
				jLocal.getName(),
				new ObjectType(refType.getClassName()));
	}
}
