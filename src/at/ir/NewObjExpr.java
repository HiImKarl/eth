package at.ir;

import soot.jimple.internal.JNewExpr;

public class NewObjExpr extends Expr {
	static int kUniqueId = 0;
	
	ObjectType type;
	int uniqueId;
	
	NewObjExpr(ObjectType type) {
		this.type = type;
		this.uniqueId = kUniqueId++;
	}
	
	NewObjExpr(NewObjExpr expr) {
		this.type = new ObjectType(expr.type);
		this.uniqueId = expr.uniqueId;
	}
	
	public int getUniqueId() {
		return this.uniqueId;
	}
	
	@Override
	public NewObjExpr clone() {
		return new NewObjExpr(this);
	}
	
	public ObjectType getType() {
		return type;
	}
	
	@Override 
	public String toString() {
		return "new " + type.toString() + "()";
	}
	
	public static NewObjExpr convertFromSoot(JNewExpr newExpr) {
		return new NewObjExpr(
				ObjectType.convertFromSoot(newExpr.getBaseType()));
	}
}
