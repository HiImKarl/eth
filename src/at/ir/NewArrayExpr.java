package at.ir;

import soot.jimple.internal.JNewArrayExpr;

public class NewArrayExpr extends Expr {
	static int kUniqueId = 0;
	Type type;
	Expr size;
	int uniqueId;
	
	public NewArrayExpr(Type type, Expr size) {
		this.type = type;
		this.size = size;
		this.uniqueId = kUniqueId++;
	}
	
	public NewArrayExpr(NewArrayExpr expr) {
		this.type = expr.type;
		this.size = expr.size;
		this.uniqueId = expr.uniqueId;
	}
	
	public Type getType() {
		return type;
	}
	
	public Expr getSize() {
		return size;
	}

	public int getUniqueId() {
		return this.uniqueId; 
	}
	
	public static NewArrayExpr convertFromSoot(JNewArrayExpr jExpr) {
		return new NewArrayExpr(
				Type.convertFromSoot(jExpr.getBaseType()),
				Expr.convertFromSoot(jExpr.getSize())
				);
	}
	
	@Override
	public NewArrayExpr clone() {
		return new NewArrayExpr(this);
	}
	
	@Override
	public String toString() {
		return "new " + type.toString() + "[" + size + "];" + uniqueId;
	}
}
