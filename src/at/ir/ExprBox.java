package at.ir;

public class ExprBox<T extends Expr> {
	T expr;
	
	public ExprBox(T expr) {
		this.expr = expr;
	}
	
	public void setExpr(T expr) {
		this.expr = expr;
	}
	
	public T getExpr() {
		return expr;
	}

}
