package at.ir;

public class ThisRef extends Variable {
	
	public ThisRef() {
		super("this");
	}
	
	public ThisRef(ThisRef param) {
		super(param);
	}

	@Override
	public ThisRef clone() {
		return new ThisRef();
	}

	@Override
	public String getName() {
		return "this";
	}

	@Override
	public String getFullName() {
		return getName();
	}


}
