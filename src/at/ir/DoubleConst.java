package at.ir;

public class DoubleConst extends Expr {
	double num;
	public DoubleConst(double num) {
		this.num = num;
	}

	public DoubleConst(DoubleConst d) {
		this.num = d.num;
	}
	
	@Override 
	public DoubleConst clone() {
		return new DoubleConst(this);
	}
	
	public double getDouble() {
		return num;
	}
	
	public void setDouble(double num) {
		this.num = num;
	}
	
	@Override 
	public String toString() {
		return Double.toString(num);
	}
}
