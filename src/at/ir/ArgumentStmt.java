package at.ir;

public class ArgumentStmt extends Stmt {
	
	// Assigns an expression to a parameter, used to assign method arguments
	// when expanding methods
	ExprBox<Parameter> lhsBox;
	ExprBox<Expr> rhsBox;
	
	public ArgumentStmt(Parameter lhs, Expr rhs) {
		lhsBox = new ExprBox<Parameter>(lhs);
		rhsBox = new ExprBox<Expr>(rhs);
	}
	
	public ArgumentStmt(ArgumentStmt stmt) {
		this.lhsBox = new ExprBox<Parameter>(stmt.getLHS().clone());
		this.rhsBox = new ExprBox<Expr>(stmt.getRHS().clone());
	}
	
	@Override 
	public ArgumentStmt clone() {
		return new ArgumentStmt(this);
	}
	
	public Parameter getLHS() {
		return lhsBox.getExpr();
	}
	
	public Expr getRHS() {
		return rhsBox.getExpr();
	}
	
	public ExprBox<Parameter> getLHSBox() {
		return lhsBox;
	}
	
	public ExprBox<Expr> getRHSBox() {
		return rhsBox;
	}
	
	@Override
	public String toString() {
		return lhsBox.getExpr().toString() + " <- " + rhsBox.getExpr().toString();
	}

}
