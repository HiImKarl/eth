package at.ir;

public class PrimitiveType extends Type {
	Primitive primitive;

	public Primitive getPrimitive() {
		return this.primitive;
	}
	
	public PrimitiveType(Primitive prim) {
		primitive = prim;
	}
	
	public PrimitiveType(PrimitiveType t) {
		this.primitive = t.primitive;
	}
	
	@Override
	public PrimitiveType clone() {
		return new PrimitiveType(this);
	}
	
	@Override
	public String toString() {
		return Type.primitiveToString(primitive);
	}
	
	public static PrimitiveType convertFromSoot(soot.PrimType type) {
		Primitive prim;
		if (type.toString().equals("boolean")) 
			prim = Primitive.BOOLEAN;
		else if (type.toString().equals("byte")) 
			prim = Primitive.BYTE;
		else if (type.toString().equals("int")) 
			prim = Primitive.INTEGER;
		else if (type.toString().equals("double")) 
			prim = Primitive.DOUBLE;
		else 
			throw new RuntimeException(type.toString());
		
		return new PrimitiveType(prim);
	}
}
