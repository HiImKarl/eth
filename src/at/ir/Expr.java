package at.ir;

import soot.Value;
import soot.jimple.BinopExpr;
import soot.jimple.ConditionExpr;
import soot.jimple.DoubleConstant;
import soot.jimple.IntConstant;
import soot.jimple.NullConstant;
import soot.jimple.ParameterRef;
import soot.jimple.SpecialInvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.internal.JArrayRef;
import soot.jimple.internal.JCastExpr;
import soot.jimple.internal.JInstanceFieldRef;
import soot.jimple.internal.JNewArrayExpr;
import soot.jimple.internal.JNewExpr;
import soot.jimple.internal.JVirtualInvokeExpr;
import soot.jimple.internal.JimpleLocal;

public abstract class Expr {
	
	@Override
	public abstract Expr clone();
	
	public static Expr convertFromSoot(Value val) {
		if (val instanceof ConditionExpr) {
			return ComparisonExpr.convertFromSoot((ConditionExpr)val);
		} else if (val instanceof BinopExpr) {
			return BinaryExpr.convertFromSoot((BinopExpr)val);
		} else if (val instanceof IntConstant) {
			return new IntConst(((IntConstant)val).value);
		} else if (val instanceof DoubleConstant) {
			return new DoubleConst(((DoubleConstant)val).value);
		} else if (val instanceof JimpleLocal) {
			return Variable.convertFromSoot((JimpleLocal)val);
		} else if (val instanceof ParameterRef) {
			return Parameter.convertFromSoot((ParameterRef)val);
		} else if (val instanceof JCastExpr) {
			return CastExpr.convertFromSoot((JCastExpr)val);
		} else if (val instanceof StaticInvokeExpr) {
			return Invoke.convertFromSoot((StaticInvokeExpr)val);
		} else if (val instanceof SpecialInvokeExpr) {
			return Invoke.convertFromSoot((SpecialInvokeExpr)val);
		} else if (val instanceof JNewArrayExpr) {
			return NewArrayExpr.convertFromSoot((JNewArrayExpr)val);
		} else if (val instanceof JArrayRef) {
			return ArrayRefVar.convertFromSoot((JArrayRef)val);
		} else if (val instanceof JNewExpr) {
			JNewExpr newExpr = (JNewExpr) val;
			return NewObjExpr.convertFromSoot(newExpr);
		} else if (val instanceof JInstanceFieldRef) {
			return FieldInstance.convertFromSoot((JInstanceFieldRef)val);
		} else if (val instanceof JVirtualInvokeExpr) {
			return VirtualInvoke.convertFromSoot((JVirtualInvokeExpr)val);
		} else if (val instanceof NullConstant) {
			return new Null();
		}
	
		throw new IllegalArgumentException("Dev forgot Value subclass: " + val.getClass().toString() + "; " + val.toString());
	}
	
	// FIXME -- convert rest of the possible exprs
	public static Value convertToStoot(Expr expr) {
		if (expr instanceof IntConst) {
			return IntConstant.v(((IntConst)expr).getInt());
		} else if (expr instanceof DoubleConst) {
			return DoubleConstant.v(((DoubleConst)expr).getDouble());
		} else if (expr instanceof BoolConst) {
			BoolConst b = (BoolConst) expr;
			return IntConstant.v(b.getInt());
		}
		throw new IllegalArgumentException("Dev forgot Expr subclass");
	}
}
