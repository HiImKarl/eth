package at.ir;

public class IntConst extends Expr {
	private int num;
	public IntConst(int num) {
		this.num = num;
	}
	
	public IntConst(IntConst i) {
		this.num = i.num;
	}
	
	@Override
	public IntConst clone() {
		return new IntConst(this);
	}
	
	public int getInt() {
		return num;
	}
	
	public void setInt(int num) {
		this.num = num;
	}
	
	@Override 
	public String toString() {
		return Integer.toString(num);
	}
}
