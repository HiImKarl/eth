package at.ir;

import soot.jimple.AddExpr;
import soot.jimple.BinopExpr;
import soot.jimple.CmpgExpr;
import soot.jimple.CmplExpr;
import soot.jimple.DivExpr;
import soot.jimple.MulExpr;
import soot.jimple.SubExpr;

public class BinaryExpr extends Expr {
	private ExprBox<Expr> lhsBox;
	private bOp op;
	private ExprBox<Expr> rhsBox;

	public enum bOp {
		ADD, SUB, MUL, DIV
	}
	
	public BinaryExpr(Expr lhs, bOp op, Expr rhs) {
		lhsBox = new ExprBox<Expr>(lhs);
		this.op = op;
		rhsBox = new ExprBox<Expr>(rhs);
	}
	
	public BinaryExpr(BinaryExpr b) {
		this.lhsBox = new ExprBox<Expr>(b.getLHS().clone());
		this.op = b.op;
		this.rhsBox = new ExprBox<Expr>(b.getRHS().clone());
	}
	
	@Override
	public BinaryExpr clone() {
		return new BinaryExpr(this);
	}
	
	public static BinaryExpr convertFromSoot(BinopExpr sootExpr) {
		Expr expr1 = convertFromSoot(sootExpr.getOp1());
		Expr expr2 = convertFromSoot(sootExpr.getOp2());
		if (sootExpr instanceof AddExpr) {
			return new BinaryExpr(expr1, bOp.ADD, expr2);
		} else if (sootExpr instanceof SubExpr) {
			return new BinaryExpr(expr1, bOp.SUB, expr2);
		} else if (sootExpr instanceof MulExpr) {
			return new BinaryExpr(expr1, bOp.MUL, expr2);
		} else if (sootExpr instanceof DivExpr) {
			return new BinaryExpr(expr1, bOp.DIV, expr2);
		} else if (sootExpr instanceof CmplExpr) {
			return new BinaryExpr(expr1, bOp.SUB, expr2);
		} else if (sootExpr instanceof CmpgExpr) {
			return new BinaryExpr(expr1, bOp.SUB, expr2);
		} else { 
			throw new IllegalArgumentException("Dev forgot case " + sootExpr.toString());
		}
	}
	
	public static String bOpToString(bOp operator) {
		switch (operator) {
		case ADD: return "+";
		case SUB: return "-";
		case MUL: return "*";
		case DIV: return "/";
		}
		throw new IllegalArgumentException("Dev forgot case statement");
	}
	
	public String getOpStr() {
		return bOpToString(op);
	}
	
	public Expr getLHS() {
		return lhsBox.getExpr();
	}
	
	public Expr getRHS() {
		return rhsBox.getExpr();
	}
	
	public ExprBox<Expr> getLHSBox() {
		return lhsBox;
	}
	
	public ExprBox<Expr> getRHSBox() {
		return rhsBox;
	}
	
	@Override
	public String toString() {
		return lhsBox.getExpr().toString() + " " + 
				bOpToString(op) + " " + rhsBox.getExpr().toString();
	}
}
