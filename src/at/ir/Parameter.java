package at.ir;

import at.Main;
import at.ir.Type.Primitive;
import soot.jimple.ParameterRef;

public abstract class Parameter extends Expr {
	protected int index;
	protected int callId = -1;
	protected int methodId;

	// Variable Name Encoding:
	// "@" + index + ":" callId + "^" methodId
	
	public Parameter(int index) {
		this.index = index;
		this.callId = -1;
		this.methodId = Main.getMethodCount();
	}
	
	public Parameter(Parameter param) {
		this.index = param.index;
		this.callId = param.callId;
		this.methodId = param.methodId;
	}
	
	@Override
	public int hashCode() {
		return getFullName().hashCode();
	}
	
	public abstract Parameter clone();
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Parameter)) return false;
		Parameter p = (Parameter)o;
		return (p.getFullName().equals(this.getFullName()));
	}
	
	public abstract String getName();
	
	public String getNameWithoutLoopId() {
		String str = "@" + index;
		str += ":" + (callId + 1); 
		str += "^" + methodId;
		return str;
	}
	
	public String getBareName() {
		return "@" + index;
	}

	public abstract String getBareNameAndType();
	
	public String getBaseName() {
		String str = "@" + index;
		str += ":" + (callId + 1); 
		str += "^" + methodId;
		return str;
	}
	
	public void setCallId(int callId) {
		this.callId = callId;
	}
	
	public int getIndex() {
		return index;
	}
	
	public abstract String getFullName();
	
	@Override
	public String toString() {
		return getFullName();
	}

	public abstract String getNameAndType();
	
	public int getMethodId() {
		return methodId;
	}
	
	public static Parameter convertFromSoot(ParameterRef pRef) {
		if (pRef.getType() instanceof soot.BooleanType) {
			return new PrimitiveParam(pRef.getIndex(), Primitive.BOOLEAN);
		} else if (pRef.getType() instanceof soot.ByteType) {
			return new PrimitiveParam(pRef.getIndex(), Primitive.BYTE);
		} else if (pRef.getType() instanceof soot.IntegerType) {
			return new PrimitiveParam(pRef.getIndex(), Primitive.INTEGER);
		} else if (pRef.getType() instanceof soot.DoubleType) {
			return new PrimitiveParam(pRef.getIndex(), Primitive.DOUBLE);
		}
		throw new IllegalArgumentException("Dev forgot case");
	}
}
