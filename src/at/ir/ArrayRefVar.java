package at.ir;

public class ArrayRefVar extends Variable {
	
	// Array reference i.e. a[i] where a is an array and i is an integer value
	ArrayType arrayType;
	Expr index; 
	Expr size;

	// ArrayRefVar Name Encoding:
	// name + ":" callId + "%" + loopId +
	// "{" + size + "}" + "[" + index + "]"
	
	public ArrayRefVar(String name, ArrayType arrayType, Expr index) {
		super(name);
		this.arrayType = arrayType;
		this.index = index;
		this.size = null;
	}
	
	public ArrayRefVar(ArrayRefVar var) {
		super(var);
		this.arrayType = new ArrayType(var.arrayType);
		this.index = var.index;
		if (var.hasSize()) this.size = var.getSize().clone();
		else this.size = null;
	}

	public boolean hasSize() {
		return size != null; 
	}
	
	public Expr getIndex() {
		return index;
	}
	
	public Expr getSize() {
		return size;
	}
	
	@Override
	public ArrayRefVar clone() {
		return new ArrayRefVar(this);
	}

	@Override
	public String getFullName() {
		return getName();
	}
	
	@Override
	public String getNameWithoutLoopId() {
		String str = super.getNameWithoutLoopId();
		str += "[" + index.toString() + "]";
		return str;
	}

	@Override
	public String getName() {
		String str = getBaseName() + "[" + index.toString() + "]{";
		if (this.hasSize()) str += size.toString();
		return str + "}";
	}

	public void setSize(Expr size) {
		this.size = size;
	}

	@Override 
	public String toString() {
		return getName() + "(" + methodId + ")|" + arrayType.toString();
	}

	public void setIndex(Expr index) {
		this.index = index;
	}
}
