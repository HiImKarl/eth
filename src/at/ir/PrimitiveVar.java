package at.ir;

import at.ir.Type.Primitive;
import soot.jimple.internal.JimpleLocal;

public class PrimitiveVar extends Variable {
	PrimitiveType primType;

	public PrimitiveType getType() {
		return primType;
	}
	
	public PrimitiveVar(String name, PrimitiveType primitive) {
		super(name);
		this.primType = primitive;
	}
	
	public PrimitiveVar(PrimitiveVar var) {
		super(var);
		this.primType = var.primType ;
	}
	
	@Override 
	public PrimitiveVar clone() {
		return new PrimitiveVar(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Variable)) return false;
		Variable v = (Variable)o;
		return (v.getFullName().equals(this.getFullName()));
	}
	
	@Override
	public String getFullName() {
		return getName();
	}
	
	public static Variable convertFromSoot(JimpleLocal jLocal) {
		Primitive primitive;
		if (jLocal.getType() instanceof soot.BooleanType)
			primitive = Primitive.BOOLEAN;
		else if (jLocal.getType() instanceof soot.ByteType)
			primitive = Primitive.BYTE;
		else if (jLocal.getType() instanceof soot.IntegerType)
			primitive = Primitive.INTEGER;
		 else if (jLocal.getType() instanceof soot.DoubleType)
			primitive = Primitive.DOUBLE;
		else 
			throw new IllegalArgumentException("Dev forgot variable type");

		return new PrimitiveVar(jLocal.getName(), new PrimitiveType(primitive));
	}

	@Override
	public String getName() {
		return getBaseName();
	}
	
	@Override 
	public String toString() {
		return getName() + "(" + methodId + ")" + "|" + primType.toString();
	}

}
