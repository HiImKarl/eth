package at.ir;

public class ThisStmt extends Stmt {

	ExprBox<ObjectVar> LHSBox;
	ExprBox<ObjectVar> RHSBox;
	
	public ThisStmt(ObjectVar var) {
		this.LHSBox = new ExprBox<ObjectVar>(var);
		this.RHSBox = new ExprBox<ObjectVar>(null);
	}
	
	public ThisStmt(ThisStmt stmt) {
		this.LHSBox = new ExprBox<ObjectVar>(new ObjectVar(stmt.LHSBox.getExpr()));
		this.RHSBox = new ExprBox<ObjectVar>(null);
		if (stmt.hasRHS()) RHSBox.setExpr(new ObjectVar(stmt.getRHS()));
	}

	@Override
	public ThisStmt clone() {
		return new ThisStmt(this);
	}
	
	public boolean hasRHS() {
		return this.RHSBox.getExpr() != null;
	}
	
	public ExprBox<ObjectVar> getLHSBox() {
		return this.LHSBox;
	}

	public ExprBox<ObjectVar> getRHSBox() {
		return this.RHSBox;
	}
	
	public ObjectVar getLHS() {
		return this.LHSBox.getExpr();
	}

	public ObjectVar getRHS() {
		return this.RHSBox.getExpr();
	}
	
	@Override 
	public String toString() {
		if (RHSBox.getExpr() == null) return LHSBox.getExpr() + "= @this";
		else return LHSBox.getExpr() + " <<= " + RHSBox.getExpr();
	}

}
