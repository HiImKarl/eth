package at.ir;

import soot.jimple.ConditionExpr;
import soot.jimple.EqExpr;
import soot.jimple.GeExpr;
import soot.jimple.GtExpr;
import soot.jimple.LeExpr;
import soot.jimple.LtExpr;
import soot.jimple.NeExpr;

public class ComparisonExpr extends Expr {
	ExprBox<Expr> lhsBox;
	cOp op;
	ExprBox<Expr> rhsBox;

	public enum cOp {
		EQ, NE, GT, GE, LT, LE
	}
	
	public ComparisonExpr(Expr lhs, cOp op, Expr rhs) {
		lhsBox = new ExprBox<Expr>(lhs);
		this.op = op;
		rhsBox = new ExprBox<Expr>(rhs);
	}
	
	public ComparisonExpr(ComparisonExpr cExpr) {
		this.lhsBox = new ExprBox<Expr>(cExpr.getLHS().clone());
		this.rhsBox = new ExprBox<Expr>(cExpr.getRHS().clone());
		this.op = cExpr.op;
	}
	
	@Override 
	public ComparisonExpr clone() {
		return new ComparisonExpr(this);
	}

	public static ComparisonExpr convertFromSoot(ConditionExpr sootCondition) {
		cOp op;
		if (sootCondition instanceof EqExpr) 
			op = cOp.EQ;
		else if (sootCondition instanceof NeExpr) 
			op = cOp.NE;
		else if (sootCondition instanceof GtExpr) 
			op = cOp.GT;
		else if (sootCondition instanceof GeExpr) 
			op = cOp.GE;
		else if (sootCondition instanceof LtExpr) 
			op = cOp.LT;
		else if (sootCondition instanceof LeExpr) 
			op = cOp.LE;
		else
			throw new IllegalArgumentException("Dev forogot case");
		
		return new ComparisonExpr(
				convertFromSoot(sootCondition.getOp1()),
				op,
				convertFromSoot(sootCondition.getOp2())
				);
	}
	
	public static String cOpToString(cOp operator) {
		switch (operator) {
		case EQ: return "==";
		case NE: return "!=";
		case GT: return ">";
		case GE: return ">=";
		case LT: return "<";
		case LE: return "<=";
		}
		throw new IllegalArgumentException("Dev forgot case statement");
	}
	
	public static cOp invertComparison(cOp operator) {
		switch (operator) {
		case EQ: return cOp.NE;
		case NE: return cOp.EQ;
		case GT: return cOp.LE;
		case GE: return cOp.LT;
		case LT: return cOp.GE;
		case LE: return cOp.GT;
		}
		throw new IllegalArgumentException("Dev forgot case statement");
	}
	
	public String getOpStr() {
		return cOpToString(op);
	}

	public Expr getLHS() {
		return lhsBox.getExpr();
	}
	
	public Expr getRHS() {
		return rhsBox.getExpr();
	}
	
	public ExprBox<Expr> getLHSBox() {
		return lhsBox;
	}
	
	public ExprBox<Expr> getRHSBox() {
		return rhsBox;
	}
	
	@Override
	public String toString() {
		return lhsBox.getExpr().toString() + " " + 
				cOpToString(op) + " " + rhsBox.getExpr().toString();
	}
}
