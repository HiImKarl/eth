package at.ir;

public class ObjectType extends Type {
	private String name;
	
	public ObjectType(String name) {
		this.name = name;
	}
	
	public ObjectType(ObjectType obj) {
		this.name = new String(obj.name);
	}

	@Override
	public ObjectType clone() {
		return new ObjectType(this);
	}

	public String getClassName() {
		return this.name;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public static ObjectType convertFromSoot(soot.RefType rType) {
		return new ObjectType(rType.getClassName());
	}
}
