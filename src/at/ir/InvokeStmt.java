package at.ir;

public class InvokeStmt extends Stmt {
	
	ExprBox<Invoke> invokeBox;
	
	public InvokeStmt(Invoke invoke) {
		this.invokeBox = new ExprBox<Invoke>(invoke);
	}
	
	
	public InvokeStmt(InvokeStmt invokeStmt) {
		this.invokeBox = new ExprBox<Invoke>(invokeStmt.getInvoke().clone());
	}

	@Override
	public InvokeStmt clone() {
		return new InvokeStmt(this);
	}

	public ExprBox<Invoke> getInvokeBox() {
		return this.invokeBox;
	}

	public Invoke getInvoke() {
		return this.invokeBox.getExpr();
	}
	
	@Override
	public String toString() {
		return invokeBox.getExpr().toString();
	}
}
