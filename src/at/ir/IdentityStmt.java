package at.ir;

public class IdentityStmt extends Stmt {
	ExprBox<Variable> lhsBox;
	ExprBox<Parameter> rhsBox;
	
	public IdentityStmt(Variable lhs, Parameter rhs) {
		lhsBox = new ExprBox<Variable>(lhs);
		rhsBox = new ExprBox<Parameter>(rhs);
	}

	public IdentityStmt(IdentityStmt stmt) {
		lhsBox = new ExprBox<Variable>(stmt.getLHS().clone());
		rhsBox = new ExprBox<Parameter>(stmt.getRHS().clone());
	}
	
	@Override
	public IdentityStmt clone() {
		return new IdentityStmt(this);
	}
	
	public Variable getLHS() {
		return lhsBox.getExpr();
	}
	
	public Parameter getRHS() {
		return rhsBox.getExpr();
	}
	
	public ExprBox<Variable> getLHSBox() {
		return lhsBox;
	}
	
	public ExprBox<Parameter> getRHSBox() {
		return rhsBox;
	}
	
	@Override
	public String toString() {
		return lhsBox.getExpr().toString() + " := " + rhsBox.getExpr().toString();
	}
}
