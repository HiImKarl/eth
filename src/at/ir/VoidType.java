package at.ir;

public class VoidType extends Type {
	
	@Override
	public Type clone() {
		return new VoidType();
	}

}
