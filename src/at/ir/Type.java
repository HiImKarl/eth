package at.ir;

import soot.PrimType;

public abstract class Type {

	public enum Primitive {
		BOOLEAN, CHAR, BYTE, INTEGER, DOUBLE
	};
	
	public static String primitiveToString(Primitive primitive) {
		switch (primitive) {
		case BYTE: return "byte";
		case BOOLEAN: return "boolean";
		case INTEGER: return "int";
		case DOUBLE: return "double";
		default:
			throw new IllegalArgumentException("Dev forgot case");
		}
	}
	
	@Override
	public abstract Type clone();
	
	public static Type convertFromSoot(soot.Type sType) {
		if (sType instanceof PrimType) {
			return PrimitiveType.convertFromSoot((PrimType)sType);
		} else if (sType instanceof soot.ArrayType) {
			return ArrayType.convertFromSoot((soot.ArrayType) sType);
		} else if (sType instanceof soot.VoidType) {
			return new VoidType();
		} else if (sType instanceof soot.RefType) {
			return ObjectType.convertFromSoot((soot.RefType)sType);
		}
		throw new IllegalArgumentException("Dev frogot type: " + sType.getClass().toString());
	}

}
