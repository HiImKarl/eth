package at.ir;

import at.Main;
import at.ir.Type.Primitive;
import soot.RefType;
import soot.Value;
import soot.jimple.internal.JArrayRef;
import soot.jimple.internal.JInstanceFieldRef;
import soot.jimple.internal.JimpleLocal;

public abstract class Variable extends Expr {
	
	// Variable Name Encoding:
	// name + ":" callId + "%" + loopId 
	
	protected String name;
	protected int callId = -1;
	protected int loopId = -1;
	protected int methodId;
	
	public Variable(String name) {
		this.name = name;
		this.callId = -1;
		this.loopId = -1;
		this.methodId = Main.getMethodCount();
	}
	
	public Variable(Variable var) {
		this.name = new String(var.name);
		this.callId = var.callId;
		this.loopId = var.loopId;
		this.methodId = var.methodId;
	}
	
	@Override 
	public int hashCode() {
		return getFullName().hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Variable)) return false;
		Variable v = (Variable)o;
		return (v.getFullName().equals(this.getFullName()));
	}
	
	public abstract Variable clone();
	
	public String getBaseName() {
		String str = this.name;
		str += ":" + (callId + 1); 
		str += "%" + (loopId + 1);
		return str;
	}

	public abstract String getName();
	
	public String getNameWithoutLoopId() {
		String str = this.name;
		str += ":" + (callId + 1); 
		return str;
	}
	
	public void setBareName(String name) {
		this.name = name;
	}
	
	public void setBaseName(Variable var) {
		this.name = var.name;
		this.callId = var.callId;
		this.loopId = var.loopId;
		this.methodId = var.methodId;
	}
	
	public abstract String getFullName();
	
	public int getMethodId() {
		return methodId;
	}
	
	public int getCallId() {
		return this.callId;
	}
	
	public void setCallId(int callId) {
		this.callId = callId;
	}
	
	public void incrLoopId() {
		throw new RuntimeException();
		//this.loopId++;
	}
	
	public static Variable convertFromSoot(JimpleLocal jLocal) {
		if (jLocal.getType() instanceof soot.BooleanType) {
			return new PrimitiveVar(jLocal.getName(), new PrimitiveType(Primitive.BOOLEAN));
		} else if (jLocal.getType() instanceof soot.ByteType) {
			return new PrimitiveVar(jLocal.getName(), new PrimitiveType(Primitive.BYTE));
		} else if (jLocal.getType() instanceof soot.IntegerType) {
			return new PrimitiveVar(jLocal.getName(), new PrimitiveType(Primitive.INTEGER));
		} else if (jLocal.getType() instanceof soot.DoubleType) {
			return new PrimitiveVar(jLocal.getName(), new PrimitiveType(Primitive.DOUBLE));
		} else if (jLocal.getType() instanceof soot.ArrayType)  {
			soot.ArrayType aType = (soot.ArrayType) jLocal.getType();
			return new ArrayVar(
					jLocal.getName(), 
					ArrayType.convertFromSoot(aType));
		} else if (jLocal.getType() instanceof soot.RefType) {
			soot.RefType rType = (RefType) jLocal.getType();
			return new ObjectVar(
					jLocal.getName(),
					ObjectType.convertFromSoot(rType));
		} else {
			throw new IllegalArgumentException("Dev forgot variable type");
		}
	}
	
	public static Variable convertFromSoot(JArrayRef arrayRef) {
		JimpleLocal base = (JimpleLocal) arrayRef.getBase();
		ArrayType arrayBaseType = (ArrayType) Type.convertFromSoot(base.getType());
		return new ArrayRefVar(
				base.getName(), 
				new ArrayType(arrayBaseType),
				Expr.convertFromSoot(arrayRef.getIndex()));
	}
	
	public static Variable convertFromSoot(Value val) {
		if (val instanceof JimpleLocal) 
			return convertFromSoot((JimpleLocal)val);
		if (val instanceof JArrayRef) 
			return convertFromSoot((JArrayRef)val);
		if (val instanceof JInstanceFieldRef)  {
			return FieldInstance.convertFromSoot((JInstanceFieldRef)val);
		}
			
		throw new IllegalArgumentException("Unsupported type: " + 
			val.getClass().toString());
	}

	@Override 
	public String toString() {
		return getFullName();
	}

	public String getBareName() {
		return this.name;
	}

	public void setLoopId(int loopId) {
		this.loopId = loopId;
	}

	public int getLoopId() {
		return this.loopId;
	}
}
