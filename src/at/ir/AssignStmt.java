package at.ir;

public class AssignStmt extends Stmt {
	ExprBox<Variable> lhsBox;
	ExprBox<Expr> rhsBox;
	
	public AssignStmt(Variable lhs, Expr rhs) {
		lhsBox = new ExprBox<Variable>(lhs);
		rhsBox = new ExprBox<Expr>(rhs);
	}
	
	public AssignStmt(AssignStmt stmt) {
		this.lhsBox = new ExprBox<Variable>(stmt.getLHS().clone());
		this.rhsBox = new ExprBox<Expr>(stmt.getRHS().clone());
	}
	
	@Override
	public AssignStmt clone() {
		return new AssignStmt(this);
	}
	
	public Variable getLHS() {
		return lhsBox.getExpr();
	}
	
	public Expr getRHS() {
		return rhsBox.getExpr();
	}
	
	public ExprBox<Variable> getLHSBox() {
		return lhsBox;
	}
	
	public ExprBox<Expr> getRHSBox() {
		return rhsBox;
	}
	
	@Override
	public String toString() {
		return lhsBox.getExpr().toString() + " = " + rhsBox.getExpr().toString();
	}
}
