package at.ir;

import java.util.ArrayList;
import java.util.List;

import at.analysis.Method;
import soot.jimple.StaticInvokeExpr;

public class Invoke extends Expr {
	String methodName;
	String className;
	List<Type> argTypes;
	Type returnType;
	List<Expr> args;
	
	public Invoke(String methodName, String className, List<Type> argTypes, Type returnType, List<Expr> args) {
		this.methodName = methodName;
		this.className = className;
		this.argTypes = argTypes;
		this.returnType = returnType;
		this.args = args;
	}
	
	public Invoke(Invoke invoke) {
		this.methodName = new String(invoke.methodName);
		this.className = new String(invoke.className);
		this.argTypes = new ArrayList<Type>(invoke.argTypes);
		this.returnType = invoke.returnType.clone();
		this.args = new ArrayList<Expr>(invoke.args);
	}
	
	@Override
	public Invoke clone() {
		return new Invoke(this);
	}
	
	public static Invoke convertFromSoot(StaticInvokeExpr expr) {
		String methodName = expr.getMethod().getSubSignature();
		String className = expr.getMethod().getDeclaringClass().getName();
		List<Type> argTypes = new ArrayList<Type>();
		for (soot.Type sootType : expr.getMethod().getParameterTypes()) 
			argTypes.add(Type.convertFromSoot(sootType));
		Type returnType = Type.convertFromSoot(expr.getMethod().getReturnType());
		List<Expr> args = new ArrayList<Expr>();
		for (soot.Value sootVal : expr.getArgs()) 
			args.add(Expr.convertFromSoot(sootVal));
		return new Invoke(methodName, className, argTypes, returnType, args);
	}

	public String getFullName() {
		return Method.methodFullName(className, methodName);
	}
	
	public List<Expr> getArgs() {
		return args;
	}
	
	@Override
	public String toString() {
		String str = "Invoke: " + this.getFullName() + " ";
		for (Expr arg : args) str += arg.toString() + ", ";
		return str;
	}
}
