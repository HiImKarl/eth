package at.ir;

public class BoolConst extends Expr {
	private boolean bool;
	
	public BoolConst(boolean bool) {
		this.bool = bool;
	}
	
	public BoolConst(BoolConst b) {
		this.bool = b.bool;
	}
	
	@Override 
	public BoolConst clone() {
		return new BoolConst(this);
	}
	
	public boolean getBool() {
		return bool;
	}
	
	public void setBool(boolean bool) {
		this.bool = bool;
	}
	
	@Override
	public String toString() {
		if (bool) return "true";
		return "false";
	}

	public int getInt() {
		if (bool) return 1;
		return 0;
	}
}
