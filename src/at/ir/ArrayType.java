package at.ir;

public class ArrayType extends Type {
	Type type;
	int numDim;
	
	public ArrayType(Type type, int numDim) {
		this.type = type;
		this.numDim = numDim;
	}
	
	public ArrayType(ArrayType aType) {
		this.type = aType.type.clone();
		this.numDim = aType.numDim;
	}
	
	public Type geType() {
		return type;
	}
	
	public int getNumDim() {
		return numDim;
	}
	
	@Override
	public ArrayType clone() {
		return new ArrayType(this);
	}
	
	@Override
	public String toString() {
		return type.toString() + "[]";
	}
	
	public static ArrayType convertFromSoot(soot.ArrayType sType) {
		return new ArrayType(
				Type.convertFromSoot(sType.getArrayElementType()),
				sType.numDimensions
				);
	}
}