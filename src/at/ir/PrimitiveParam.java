package at.ir;

import at.ir.Type.Primitive;

public class PrimitiveParam extends Parameter {
	PrimitiveType primType;
	
	public PrimitiveParam(int index, Primitive prim) {
		super(index);
		this.primType = new PrimitiveType(prim);
	}
	
	public PrimitiveType getType() {
		return primType;
	}
	
	public PrimitiveParam(PrimitiveParam param) {
		super(param);
		this.primType = param.primType;
	}

	@Override
	public Parameter clone() {
		return new PrimitiveParam(this);
	}

	@Override
	public String getName() {
		return getBaseName();
	}

	@Override
	public String getBareNameAndType() {
		return getBareName() + ": " + primType.toString();
	}

	@Override
	public String getFullName() {
		return getName();
	}

	@Override
	public String getNameAndType() {
		return getName() + "|" + primType.toString();
	}
	
	@Override 
	public String toString() {
		return getNameAndType();
	}
}
