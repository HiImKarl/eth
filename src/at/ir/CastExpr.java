package at.ir;

import soot.jimple.internal.JCastExpr;

public class CastExpr extends Expr {
	private ExprBox<Expr> exprBox;
	private Type castType;
	
	public CastExpr(Expr expr, Type castType) {
		this.exprBox = new ExprBox<Expr>(expr);
		this.castType = castType;
	}
	
	public CastExpr(CastExpr cExpr) {
		this.exprBox = new ExprBox<Expr>(cExpr.getExpr().clone());
		this.castType = cExpr.getCastType().clone();
	}
	
	@Override
	public CastExpr clone() {
		return new CastExpr(this);
	}
	
	public Expr getExpr() {
		return exprBox.getExpr();
	}
	
	public ExprBox<Expr> getExprBox() {
		return exprBox;
	}
	
	public Type getCastType() {
		return castType;
	}
	
	public static CastExpr convertFromSoot(JCastExpr jCastExpr) {
		if (jCastExpr.getOp() == null) throw new RuntimeException("!!");
		return new CastExpr(
				Expr.convertFromSoot(jCastExpr.getOp()),
				Type.convertFromSoot((soot.Type)jCastExpr.getCastType())
				);
	}
	
	@Override
	public String toString() {
		return "(" + castType.toString() + ")" + exprBox.getExpr().toString();
	}
}
