package at.ir;


public class ArrayVar extends Variable {
	ArrayType arrayType;
	Expr size;
	// This field is used to resolve object reference equivalence
	int uniqueId = -1;

	// ArrayRefVar Name Encoding:
	// name + ":" callId + "%" + loopId + "{" + size + "}" 

	public ArrayVar(String name, ArrayType aType) {
		super(name);
		this.arrayType = aType;
		this.size = null;
	}
	
	public boolean hasSize() {
		return size != null; 
	}
	
	public Expr getSize() {
		return size;
	}
	
	public void setSize(Expr size) {
		this.size = size;
	}
	
	public ArrayVar(ArrayVar var) {
		super(var);
		this.arrayType = new ArrayType(var.arrayType);
		if (var.hasSize()) this.size = var.getSize().clone();
		this.uniqueId = var.uniqueId;
	}
	
	@Override 
	public ArrayVar clone() {
		return new ArrayVar(this);
	}

	@Override
	public String getFullName() {
		return this.getName();
	}

	@Override
	public String getName() {
		String str = getBaseName();
		if (hasSize()) str += "{" + size.toString() + "}";
		return str;
	}

	public ArrayType getArrayType() {
		return arrayType;
	}
	
	public int getUniqueId() {
		return this.uniqueId;
	}
	
	public void setUniqueId(int uniqueId) {
		this.uniqueId = uniqueId;
	}
	
	public boolean isUniqueIdSet() {
		return uniqueId != -1;
	}
	
	@Override 
	public String toString() {
		return getFullName() + "(" + methodId + ")|" + arrayType.toString() + ";" + uniqueId;
	}
}
