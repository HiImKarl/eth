package at.ir;

import soot.RefType;
import soot.jimple.internal.JInstanceFieldRef;
import soot.jimple.internal.JimpleLocal;

public class FieldInstance extends Variable {
	private ObjectType baseClass;
	private Variable field;
	
	public FieldInstance(String name, ObjectType baseClass, Variable field) {
		super(name);
		this.baseClass = baseClass;
		this.field = field;
	}
	
	public FieldInstance(ObjectVar obj, Variable field) {
		super(obj);
		this.baseClass = obj.getType().clone();
		this.field = field.clone();
	}
	
	public FieldInstance(FieldInstance fieldInstance) {
		super(fieldInstance);
		this.baseClass = fieldInstance.baseClass.clone();
		this.field = fieldInstance.field.clone();
	}
	
	@Override
	public FieldInstance clone() {
		return new FieldInstance(this);
	}
	
	public ObjectType getBaseClass() {
		return baseClass;
	}
	
	public Variable getLastField() {
		FieldInstance instance = this;
		while (instance.getField() instanceof FieldInstance) 
			instance = (FieldInstance) instance.getField();
		return instance.getField();
	}
	
	public void setLastField(Variable lastField) {
		FieldInstance instance = this;
		while (instance.getField() instanceof FieldInstance) 
			instance = (FieldInstance) instance.getField();
		instance.setField(lastField);
	}
	
	public Variable getField() {
		return field;
	}
	
	public void setField(Variable field) {
		this.field = field;
	}

	public static FieldInstance convertFromSoot(JInstanceFieldRef fieldRef) {
		if (!(fieldRef.getBase() instanceof JimpleLocal))
			throw new RuntimeException("Base must be JimpleLocal, not " + fieldRef.getBase().getClass().toString());
		if (!(fieldRef.getBase().getType() instanceof RefType))
			throw new RuntimeException("Base must be a RefType, not " + fieldRef.getBase().getClass().toString());
		JimpleLocal jLocal = (JimpleLocal) fieldRef.getBase();
		if (fieldRef.getType() instanceof soot.PrimType) {
			return new FieldInstance(jLocal.getName(), 
				ObjectType.convertFromSoot((RefType)fieldRef.getBase().getType()),
				new PrimitiveVar(fieldRef.getFieldRef().name(),
				PrimitiveType.convertFromSoot((soot.PrimType)fieldRef.getType())));
		} else if (fieldRef.getType() instanceof soot.RefType) {
			return new FieldInstance(jLocal.getName(), 
				ObjectType.convertFromSoot((RefType)fieldRef.getBase().getType()),
				new ObjectVar(fieldRef.getFieldRef().name(),
				ObjectType.convertFromSoot((soot.RefType)fieldRef.getType())));
		}
		throw new IllegalArgumentException("Not supported: " + fieldRef.getType().toString());
	}

	@Override
	public String getNameWithoutLoopId() {
		String str = super.getNameWithoutLoopId();
		str += "." + field;
		return str;
	}
	
	@Override 
	public String toString() {
		return getName() + "(" + methodId + ")" ;
	}
	
	public String getFullBaseName() {
		String str = getBaseName();
		FieldInstance nextField = this;
		while (nextField.getField() instanceof FieldInstance) {
			nextField = (FieldInstance) nextField.getField();
			str += "." + nextField.getBaseName();
		}
		return str;
	}

	@Override
	public String getName() {
		return getBaseName() + "." + field.getName();
	}

	@Override
	public String getFullName() {
		return getName();
	}

}
