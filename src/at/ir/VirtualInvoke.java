package at.ir;

import java.util.ArrayList;
import java.util.List;

import soot.jimple.SpecialInvokeExpr;
import soot.jimple.VirtualInvokeExpr;
import soot.jimple.internal.JimpleLocal;

public class VirtualInvoke extends Invoke {
	
	ObjectVar thisVar;

	public VirtualInvoke(ObjectVar thisVar, String methodName, String className, List<Type> argTypes, Type returnType, List<Expr> args) {
		super(methodName, className, argTypes, returnType, args);
		this.thisVar = thisVar;
	}

	public VirtualInvoke(VirtualInvoke invoke) {
		super(invoke);
		this.thisVar = new ObjectVar(invoke.thisVar);
	}
	
	@Override
	public VirtualInvoke clone() {
		return new VirtualInvoke(this);
	}
	
	public ObjectVar getThisVar() {
		return thisVar;
	}
	
	public static VirtualInvoke convertFromSoot(VirtualInvokeExpr expr) {
		String methodName = expr.getMethod().getSubSignature();
		String className = expr.getMethod().getDeclaringClass().getName();
		List<Type> argTypes = new ArrayList<Type>();
		for (soot.Type sootType : expr.getMethod().getParameterTypes())
			argTypes.add(Type.convertFromSoot(sootType));
		Type returnType = Type.convertFromSoot(expr.getMethod().getReturnType());
		List<Expr> args = new ArrayList<Expr>();
		for (soot.Value sootVal : expr.getArgs())
			args.add(Expr.convertFromSoot(sootVal));
		ObjectVar var = ObjectVar.convertFromSoot((JimpleLocal)expr.getBase());
		return new VirtualInvoke(var, methodName, className, argTypes, returnType, args);
	}

	public static VirtualInvoke convertFromSoot(SpecialInvokeExpr expr) {
		String methodName = expr.getMethod().getSubSignature();
		String className = expr.getMethod().getDeclaringClass().getName();
		List<Type> argTypes = new ArrayList<Type>();
		for (soot.Type sootType : expr.getMethod().getParameterTypes()) 
			argTypes.add(Type.convertFromSoot(sootType));
		Type returnType = Type.convertFromSoot(expr.getMethod().getReturnType());
		List<Expr> args = new ArrayList<Expr>();
		for (soot.Value sootVal : expr.getArgs())
			args.add(Expr.convertFromSoot(sootVal));
		ObjectVar var = ObjectVar.convertFromSoot((JimpleLocal)expr.getBase());
		return new VirtualInvoke(var, methodName, className, argTypes, returnType, args);
	}

}
