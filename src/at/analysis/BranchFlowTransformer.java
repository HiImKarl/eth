package at.analysis;

import java.util.Map;

import soot.*;
import soot.toolkits.graph.BriefBlockGraph;

// For each method, construct its Graph and add it to the global graph pool
public class BranchFlowTransformer extends BodyTransformer {
	private static BranchFlowTransformer instance = new BranchFlowTransformer();
	public static BranchFlowTransformer v() { return instance; }
	private BranchFlowTransformer() {}
	
	@Override
	// This function applies to each method
	protected void internalTransform(Body body, String phaseName,
			Map<String, String> options) {
		SootMethod sMethod = body.getMethod();
		if (sMethod.getSubSignature().equals("void <init>()")) return;
		if (sMethod.getSubSignature().equals("void main(java.lang.String[])")) return;
		// FIXME this graph does not handle exceptions
		BriefBlockGraph graph = new BriefBlockGraph(body);
		at.Main.graphs.add(graph);

	}
}
