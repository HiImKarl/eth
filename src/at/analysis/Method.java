package at.analysis;

import java.util.*;

import at.Main;
import at.eval.Reducer;
import at.eval.Simplify;
import at.eval.Solver;
import soot.SootMethod;
import soot.Value;
import soot.jimple.ParameterRef;
import at.ir.BoolConst;
import at.ir.DoubleConst;
import at.ir.Expr;
import at.ir.IntConst;
import at.ir.Parameter;
import at.ir.PrimitiveVar;

public class Method {
	// Bundles all of the information for a single 
	// method produced during branch analysis 
	private List<ProgramPath> solvedPaths;
	private List<ProgramPath> possiblePaths;
	private List<ProgramPath> expandedPaths;
	private List<Parameter> parameters;
	private Set<Integer> possibleTags;
	private String className;
	private String name;
	private SootMethod sootMethod;
	private int uniqueId;
	
	@Override
	public int hashCode() {
		return this.getFullName().hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Method)) return false;
		Method m = (Method)o;
		return this.getFullName().equals(m.getFullName());
	}
	
	public int getId() {
		return uniqueId;
	}
	
	public Set<Integer> getTags() {
		return possibleTags;
	}
	
	public boolean hasSolutions() {
		return solvedPaths.size() > 0;
	}
	
	public boolean hasPrimitiveReturnType() {
		Expr expr = expandedPaths.get(0).getReturnValue();
		if (expr instanceof BoolConst) return true;
		if (expr instanceof IntConst) return true;
		if (expr instanceof DoubleConst) return true;
		if (expr instanceof PrimitiveVar) return true;
		return false;
	}

	public boolean isConstructor() {
		return (name.contains("<init>"));
	}
	
	public int getNumTags() {
		return possibleTags.size();
	}
	
	public List<ProgramPath> getPaths() {
		return solvedPaths;
	}
	
	public List<ProgramPath> getPossiblePaths() {
		return possiblePaths;
	}
	
	public List<ProgramPath> getExpandedPaths() {
		return expandedPaths;
	}
	
	public void setExpandedPaths(List<ProgramPath> paths) {
		this.expandedPaths = paths;
	}
	
	public void setPaths(List<ProgramPath> paths) {
		this.possiblePaths = paths;
	}
	
	public int numParameters() {
		return parameters.size();
	}
	
	public List<Parameter> getParameters() {
		return parameters;
	}
	
	public SootMethod getSootMethod() {
		return sootMethod;
	}
	
	public static String methodFullName(String className, String methodName) {
		return "<" + className + ": " + methodName + ">";
 
	}
	
	public String getFullName() {
		return methodFullName(className, name);
	}
	
	public Method(List<ProgramPath> paths, List<Value> parameterRefs, Set<Integer> possibleTags, 
			String name, String className, SootMethod sootMethod) {
		this.solvedPaths = new ArrayList<ProgramPath>();
		this.possiblePaths = paths;
		this.expandedPaths = new ArrayList<ProgramPath>(paths);
		this.parameters = new ArrayList<Parameter>();
		this.possibleTags = possibleTags;
		this.name = name;
		this.className = className;
		this.sootMethod = sootMethod;
		this.uniqueId = Main.getMethodCount();
		
		for (Value val: parameterRefs) {
			if (!(val instanceof ParameterRef))
				throw new RuntimeException("paramterRefs must be a list of only parameters");
			ParameterRef pRef = (ParameterRef) val;
			parameters.add(Parameter.convertFromSoot(pRef));
		}
	}
	
	public static Expr[] solutionsList(Map<Parameter, Expr> solutionSet) {
		Expr[] sols = new Expr[solutionSet.size()];
		for (Map.Entry<Parameter, Expr> entry : solutionSet.entrySet()) {
			sols[entry.getKey().getIndex()] = entry.getValue();
		}
		return sols;
	}
	
	private int getNextPath(List<ProgramPath> pathsToSolve, Reducer reducer) {
		int index = -1;
		int max_tags = 0;
		for (int i = 0; i < pathsToSolve.size(); ++i) {
			int tags_covered = reducer.numUncovered(pathsToSolve.get(i).getTags());
			if (tags_covered > max_tags) {
				max_tags = tags_covered;
				index = i;
			}
		}
		return index;
	}
	
	// Simplifies and solves all of the methods in expandedPaths and places them
	// into solvedPaths
	public void solve() {
		Reducer reducer = new Reducer();
		List<ProgramPath> pathsToSolve = new ArrayList<ProgramPath>();
		for (ProgramPath path: expandedPaths) 
			pathsToSolve.add(path);
		int nextPathIndex = getNextPath(pathsToSolve, reducer);
		while (nextPathIndex != -1 && pathsToSolve.size() > 0) {
			ProgramPath path = pathsToSolve.get(nextPathIndex);
			pathsToSolve.remove(nextPathIndex);
			Simplify.v().simplifyConditions(path);
			Map<Parameter, Expr> solutionSet = Solver.v().solve(path.getConditions(), parameters);
			if (solutionSet != null) {
				path.setSolutions(solutionSet);
				solvedPaths.add(path);
				reducer.insertCoverage(path.getTags());
			}
			nextPathIndex = getNextPath(pathsToSolve, reducer);
		}
	}
	
	public String getName() {
		return name;
	}
	
	public String getClassName() {
		return className;
	}
	
	public boolean isSolved() {
		return solvedPaths.size() != 0;
	}
	
	@Override 
	public String toString() {
		String str = "Method: " + this.getFullName() + "\n";
		str += "Tags:";
		for (Integer tag : possibleTags) str += " " + tag;
		str += "\nParameters:\n";
		for (Parameter parameter : parameters)
			str += "\t" + parameter.getNameAndType() + "\n";
		str += "Program Paths:\n";

		for (ProgramPath path: solvedPaths)
			str += path.toString();
		return str;
	}
}
