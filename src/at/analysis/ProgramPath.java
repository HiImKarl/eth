package at.analysis;

import java.util.*;

import soot.Unit;
import soot.Value;
import soot.jimple.IdentityRef;
import soot.jimple.InvokeExpr;
import soot.jimple.ParameterRef;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.SpecialInvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.VirtualInvokeExpr;
import soot.jimple.internal.JAssignStmt;
import soot.jimple.internal.JIdentityStmt;
import soot.jimple.internal.JInvokeStmt;
import soot.jimple.internal.JimpleLocal;
import soot.shimple.PhiExpr;
import soot.toolkits.graph.Block;
import at.Main;
import at.ir.*;
import at.util.Tags;

public class ProgramPath {
	// A single possible execution path for a method
	
	private List<Stmt> path;
	private List<ComparisonExpr> conditions;
	private Set<Integer> tags;
	private Map<Parameter, Expr> solutions;
	private Expr returnValue;

	private Set<String> methodDependencies;
	private int numMethodCalls;
	private int prevNumMethodCalls = -1;
	private Map<Method, Integer> methodCallCounter;
	
	public Set<String> getMethodDependencies() {
		return this.methodDependencies;
	}
	
	public int getNumMethodCalls() {
		return this.numMethodCalls;
	}

	public int getPrevNumMethodCalls() {
		return this.prevNumMethodCalls;
	}
	
	public void incrNumMethodCalls() {
		++this.numMethodCalls;
	}
	
	public Map<Method, Integer> getMethodCallCounter() {
		return this.methodCallCounter;
	}
	
	public int getMethodCallCount(Method method) {
		if (methodCallCounter.containsKey(method))
			return methodCallCounter.get(method);
		return 0;
	}
	
	public int incrMethodCallCount(Method method) {
		if (methodCallCounter.containsKey(method)) {
			int count = methodCallCounter.get(method) + 1;
			methodCallCounter.put(method, count);
			return count;
		}
		methodCallCounter.put(method, 1);
		return 1;
	}
	
	public ProgramPath(List<Stmt> path, List<ComparisonExpr> conditions, 
			Set<Integer> tags, Expr returnValue, Set<String> methodDependencies, 
			int numMethodCalls, int prevNumMethodCalls,
			Map<Method, Integer> methodCounter) {
		this.path = path;
		this.conditions = conditions;
		this.tags = tags;
		this.solutions = null;
		this.methodDependencies = methodDependencies;
		this.returnValue = returnValue;
		this.numMethodCalls = numMethodCalls;
		this.prevNumMethodCalls = prevNumMethodCalls;
		this.methodCallCounter = new HashMap<Method, Integer>(methodCounter);
	}
	
	public List<Stmt> getPath() {
		return path;
	}
	
	public int getNumTags() {
		return tags.size();
	}
	
	public Set<Integer> getTags() {
		return tags;
	}
	
	public List<ComparisonExpr> getConditions() {
		return conditions;
	}
	
	public void setConditions(List<ComparisonExpr> conditions) {
		this.conditions = conditions;
	}
	
	// After unwinding loops, SSA is unpreserved, so convert back
	// This method converts RHS of statements into SSA
	private void rhsLoopSSA(Expr rhs, Map<String, Integer> ssaCounter) {
		if (rhs instanceof BinaryExpr) {
			BinaryExpr binExpr = (BinaryExpr) rhs;
			rhsLoopSSA(binExpr.getLHS(), ssaCounter);
			rhsLoopSSA(binExpr.getRHS(), ssaCounter);
		} else if (rhs instanceof Variable) {
			Variable var = (Variable) rhs;
			String varName = var.getNameWithoutLoopId();
			if (ssaCounter.containsKey(varName)) {
				int loopCount = ssaCounter.get(varName);
				if (loopCount >= 0)
					var.setLoopId(loopCount);
			}
		} else if (rhs instanceof Invoke) {
			Invoke invoke = (Invoke) rhs;
			for (Expr arg : invoke.getArgs()) {
				if (!(arg instanceof Variable)) continue;
				Variable var = (Variable) arg;
				rhsLoopSSA(var, ssaCounter);
			}
		}
	}
	
	// After unwinding loops, SSA is unpreserved, so convert back
	// This method converts a single statement into SSA by registering
	// repeating variables on the LHS and counting how many times they occur
	private void loopSSA(AssignStmt assignExpr, Map<String, Integer> ssaCounter) {
		Variable varNode = assignExpr.getLHS();
		rhsLoopSSA(assignExpr.getRHS(), ssaCounter);
		String varName = varNode.getNameWithoutLoopId();
		if (!ssaCounter.containsKey(varName)) {
			ssaCounter.put(varName, -1);
		} else {
			int loopCount = ssaCounter.get(varName);
			++loopCount;
			varNode.setLoopId(loopCount);
			ssaCounter.put(varName, loopCount);
		}
	}

	// Constructor performs the conversion between Soot's IR with
	// this tool's IR
	public ProgramPath(Stack<AnalysisBlock> blocks) {
		this.path = new ArrayList<Stmt>();
		this.conditions = new ArrayList<ComparisonExpr>();
		this.tags = new HashSet<Integer>();
		this.solutions = null;
		this.numMethodCalls = 0;
		this.methodDependencies = new HashSet<String>();
		this.methodCallCounter = new HashMap<Method, Integer>();
		Map<String, Integer> ssaCounter = new HashMap<String, Integer>();

		Block prevBlock = null;
		for (AnalysisBlock analysisBlock : blocks) {
			Block sootBlock = analysisBlock.getSootBlock();
			Iterator<Unit> uIt = sootBlock.iterator();
			while (uIt.hasNext()) {
				Unit unit = uIt.next();
				Stmt nextStmt = null;
				if (unit instanceof JAssignStmt) {
					JAssignStmt assignStmt = (JAssignStmt) unit;
					AssignStmt aStmt;
					if (assignStmt.rightBox.getValue() instanceof PhiExpr) {
						PhiExpr pExpr = (PhiExpr) assignStmt.rightBox.getValue();
						Value val = pExpr.getValue(prevBlock);
						if (val == null) throw new RuntimeException("\nBlock: \n" + prevBlock.toString()
								+ " | Phi: " + pExpr.toString());
						aStmt = new AssignStmt(
								Variable.convertFromSoot(assignStmt.getLeftOp()), 
								Expr.convertFromSoot(val));
					} else {
						aStmt = new AssignStmt(
								Variable.convertFromSoot(assignStmt.getLeftOp()), 
								Expr.convertFromSoot(assignStmt.getRightOp()));
					}
					loopSSA(aStmt, ssaCounter);
					nextStmt = aStmt;
				} else if (unit instanceof JIdentityStmt) {
					JIdentityStmt identityStmt = (JIdentityStmt)unit;
					Value val = identityStmt.rightBox.getValue();
					if (val instanceof ParameterRef) {
						nextStmt = new IdentityStmt(
								Variable.convertFromSoot((JimpleLocal)identityStmt.getLeftOp()),
								Parameter.convertFromSoot((ParameterRef)val)
								);
					} else if (val instanceof IdentityRef) {
						nextStmt = new ThisStmt(
								(ObjectVar) Variable.convertFromSoot((JimpleLocal)identityStmt.getLeftOp())
								);
					} else {
						throw new RuntimeException(identityStmt.toString());
					}
				} else if (unit instanceof JInvokeStmt) {
					InvokeExpr invokeExpr = ((JInvokeStmt) unit).getInvokeExpr();
					if (invokeExpr.getMethod().getDeclaringClass().getName().equals("coverage.Coverage"))
						continue;
					if (invokeExpr.getMethod().getSignature().equals("<java.lang.Object: void <init>()>"))
						continue;
					InvokeStmt invokeStmt;
					if (invokeExpr instanceof StaticInvokeExpr)
						invokeStmt = new InvokeStmt(Invoke.convertFromSoot((StaticInvokeExpr) invokeExpr));
					else if (invokeExpr instanceof SpecialInvokeExpr)
						invokeStmt = new InvokeStmt(VirtualInvoke.convertFromSoot((SpecialInvokeExpr) invokeExpr));
					else if (invokeExpr instanceof VirtualInvokeExpr)
						invokeStmt = new InvokeStmt(VirtualInvoke.convertFromSoot((VirtualInvokeExpr) invokeExpr));
					else
						throw new RuntimeException();
					nextStmt = invokeStmt;
					rhsLoopSSA(invokeStmt.getInvoke(), ssaCounter);
					methodDependencies.add(((InvokeStmt)nextStmt).getInvoke().getFullName());
				}

				if (nextStmt != null) {
					this.recordInvokeStmt(nextStmt);
					path.add(nextStmt);
				}
			}
			boolean conditionSkipped = insertCondition(analysisBlock, ssaCounter);
			prevBlock = analysisBlock.getSootBlock();
			if (!conditionSkipped) tags.add(Tags.v().getTag(analysisBlock.getSootBlock()));
		}
		
		Block lastBlock = blocks.lastElement().getSootBlock();
		Unit lastUnit = lastBlock.getTail();
		if (lastUnit instanceof ReturnStmt) {
			ReturnStmt returnStmt = (ReturnStmt)lastUnit;
			this.returnValue = Expr.convertFromSoot(returnStmt.getOp());
		} else if (lastUnit instanceof ReturnVoidStmt) {
			this.returnValue = null;
		} else {
			throw new IllegalArgumentException("Last stmt should be a return stmt");
		}
	}
	
	public boolean hasReturnValue() {
		return returnValue == null;
	}
	
	public Expr getReturnValue() {
		return returnValue;
	}
	
	// If stmt contains an InvokeStmt, add it to the 
	// method dependencies set
	public void recordInvokeStmt(Stmt stmt) {
		if (stmt instanceof AssignStmt) {
			Expr rhs = ((AssignStmt)stmt).getRHS();
			if (rhs instanceof Invoke) {
				Invoke invoke = (Invoke) rhs;
				this.methodDependencies.add(invoke.getFullName());
			}
		} 
	}
	
	// If the loop iteration maximum is reached, skip
	// Returns a boolean indicating if the condition has been skipped
	private boolean insertCondition(AnalysisBlock analysisBlock, Map<String, Integer> ssaCounter) {
		if (analysisBlock.hasCondition()) {
			ComparisonExpr cExpr = ComparisonExpr.convertFromSoot(analysisBlock.getCondition());
			rhsLoopSSA(cExpr.getLHS(), ssaCounter);
			rhsLoopSSA(cExpr.getRHS(), ssaCounter);
			if (cExpr.getLHS() instanceof Variable) {
				Variable var = (Variable) cExpr.getLHS();
				int loopCount = var.getLoopId();
				if (loopCount == Main.loopIterationMaximum) 
					return true;
			}
			conditions.add(cExpr);
		}
		return false;
	}

	public boolean hasSolutions() {
		return solutions != null;
	}
	
	public Map<Parameter, Expr> getSolutions() {
		return solutions;
	}
	
	public void setSolutions(Map<Parameter, Expr> solutions) {
		this.solutions = solutions;
	}
	
	public boolean containsInvoke() {
		return this.methodDependencies.size() > 0;
	}
	
	@Override
	public String toString() {
		String str = "\tTags: ";
		for (Integer tag : tags)
			str += tag + " ";
		str += "\n\tConditions:\n";
		for (ComparisonExpr condition : conditions) 
			str += "\t\t" + condition.toString() + "\n";
		str += "\tPath:\n";
		for (Stmt stmt : path) 
			str += "\t\t" + stmt.toString() + "\n";
		if (returnValue != null)
			str += "ReturnValue: " + returnValue.toString() + "\n";
		else
			str += "ReturnValue: void\n";
		str += "Method calls:\n";
		for (Map.Entry<Method, Integer> entry: methodCallCounter.entrySet())
			str += "\t" + entry.getKey().getFullName() + ": " + entry.getValue() + "\n";
		if (hasSolutions()) {
			str += "Solution Set:";
			Map<Parameter, Expr> solutions = getSolutions();
			for (Map.Entry<Parameter, Expr> entry : solutions.entrySet())
				str += entry.getKey().toString() + " = " + entry.getValue().toString() + ",";
			str += "\n\n";
		} else {
			str += "No solution can be found\n\n";
		}
		return str;
	}
}
