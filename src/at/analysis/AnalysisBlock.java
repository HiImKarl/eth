package at.analysis;

import soot.Unit;
import soot.jimple.ConditionExpr;
import soot.toolkits.graph.Block;

public class AnalysisBlock {
	// Tuple of objects used by at.analysis.BranchFlowAnalysis
	// during program branch creation
	private Block sootBlock;
	private ConditionExpr condition;
	private Unit branchSource;

	public AnalysisBlock(Block sootBlock, ConditionExpr condition, 
			Unit branchSource) {
		this.sootBlock = sootBlock;
		this.condition = condition;
		this.branchSource = branchSource;
	}
	
	public Block getSootBlock() {
		return sootBlock;
	}
	
	public boolean hasCondition() {
		return condition != null;
	}
	
	public ConditionExpr getCondition() {
		return condition;
	}
	
	public Unit getBranchSource() {
		return branchSource;
	}
}
