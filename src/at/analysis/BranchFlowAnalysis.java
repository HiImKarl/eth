package at.analysis;

import java.util.*;

import at.Main;
import at.util.Tags;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.jimple.*;
import soot.jimple.internal.*;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.BriefBlockGraph;

public class BranchFlowAnalysis {
	private static BranchFlowAnalysis instance = new BranchFlowAnalysis();
	private BranchFlowAnalysis() {}
	public static BranchFlowAnalysis v() { return instance; }
	
	private Stack<AnalysisBlock> currentPath;
	private Map<Block, Integer> loopTracker;
	private List<ProgramPath> possiblePaths;
	private BriefBlockGraph graph;

	// Inverts the logical comparison i.e. x >= y --> x < y
	private static ConditionExpr inverseCondition(ConditionExpr condition) {
		if (condition instanceof EqExpr) {
			return new JNeExpr(condition.getOp1(), condition.getOp2());
		} else if (condition instanceof NeExpr) {
			return new JEqExpr(condition.getOp1(), condition.getOp2());
		} else if (condition instanceof GeExpr) {
			return new JLtExpr(condition.getOp1(), condition.getOp2());
		} else if (condition instanceof GtExpr) {
			return new JLeExpr(condition.getOp1(), condition.getOp2());
		} else if (condition instanceof LeExpr) {
			return new JGtExpr(condition.getOp1(), condition.getOp2());
		} else if (condition instanceof LtExpr) {
			return new JGeExpr(condition.getOp1(), condition.getOp2());
		}
		return null;
	}
	
	private int addBlockToLoopCount(Block block) {
		if (loopTracker.containsKey(block)) {
			int count = loopTracker.get(block);
			loopTracker.put(block, count + 1);
			return count;
		} else {
			loopTracker.put(block, 0);
			return 0;
		}
	}
	
	private void removeBlockFromLoopCount(Block block) {
		if (loopTracker.get(block) == 0)
			loopTracker.remove(block);
		else 
			loopTracker.put(block, loopTracker.get(block) - 1);
	}
	
	private void addAnalysisBlock(Block block, ConditionExpr condition, Unit branchSource) {
		addBlockToLoopCount(block);
		AnalysisBlock aBlock = new AnalysisBlock(block, condition, branchSource);
		currentPath.add(aBlock);
		constructProgramPaths(aBlock.getSootBlock());
		currentPath.pop();
		removeBlockFromLoopCount(block);
	}
	
	// Add the current block to the paths constructed so far and 
	// copy the current paths into the existing path pool if 
	// the current block terminates.
	private void constructProgramPaths(Block currBlock) {
		List<Block> successors = graph.getSuccsOf(currBlock);
		if (successors.isEmpty()) {
			int branchCount = 0;
			if (loopTracker.containsKey(currBlock))
				branchCount = loopTracker.get(currBlock);
			if (branchCount == 0 || branchCount > Main.loopIterationMinimum)
				possiblePaths.add(new ProgramPath(currentPath));
			return;
		}
		
		Unit branchUnit = currBlock.getTail();
		// FIXME -- only considering if statements, not switches
		if (branchUnit instanceof IfStmt) {
			ConditionExpr condition = (ConditionExpr) ((IfStmt)branchUnit).getCondition();
			Block fallThrough = graph.getSuccsOf(currBlock).get(0);
			addAnalysisBlock(fallThrough, inverseCondition(condition), branchUnit);
			
			Block branchOff = graph.getSuccsOf(currBlock).get(1);
			addAnalysisBlock(branchOff, condition, branchUnit);
		} else {
			Block branchOff = graph.getSuccsOf(currBlock).get(0);
			if (loopTracker.containsKey(branchOff)) {
				int branchCount = loopTracker.get(branchOff);
				if (branchCount > Main.loopIterationMaximum) return;
			}
			
			addAnalysisBlock(branchOff, null, branchUnit);
		}
	}
	
	// Recursively create all of the possible program execution paths 
	// for a method using program its flow graph 
	public Method analyze(BriefBlockGraph graph) {
		this.possiblePaths = new ArrayList<ProgramPath>();
		this.currentPath = new Stack<AnalysisBlock>();
		this.loopTracker = new HashMap<Block, Integer>(); 
		this.graph = graph;
		
		if (graph.getHeads().size() != 1) 
			throw new RuntimeException("Graph has more than one point of entry??");
		Block startBlock = graph.getHeads().get(0);
		currentPath.add(new AnalysisBlock(startBlock, null, null));
		constructProgramPaths(startBlock);
		
		SootMethod sMethod = graph.getBody().getMethod();
		SootClass sClass = sMethod.getDeclaringClass();
		List<Value> parameterRefs = graph.getBody().getParameterRefs();
		return new Method(possiblePaths, parameterRefs, Tags.v().getTags(), 
				sMethod.getSubSignature(), sClass.getName(), sMethod);
	}
}