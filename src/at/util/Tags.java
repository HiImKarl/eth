package at.util;

import java.util.*;

import soot.SootMethod;
import soot.Unit;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.IntConstant;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.BriefBlockGraph;

public class Tags {
	private static Tags instance = new Tags();
	public static Tags v() { return instance; }
	private Tags() {}
	
	private final static String addTagSignature = "void addTag(int)";
	// FIXME -- Using Block::toString() is VERY costly for hashing
	private Map<String, Integer> tagMap;
	private Map<Integer, List<Integer>> tagGraph;
	
	public void addTagToMap(Block cond, int tag) {
		if (cond == null) throw new RuntimeException("Block is null");
		tagMap.put(cond.toString(), tag);
	}
	
	public int getNumTags() {
		return tagMap.size();
	}
	
	public Set<Integer> getTags() {
		Set<Integer> tags = new HashSet<Integer>();
		for (Map.Entry<String, Integer> entry : tagMap.entrySet())
			tags.add(entry.getValue());
		return tags;
	}
	
	// FIXME
	public Set<Integer> getUniqueTags(Set<Integer> tags) {
		return null;
	}
	
	public void addTagToGraph(int tagKey, int tagVal) {
		if (!tagGraph.containsKey(tagKey)) {
			tagGraph.put(tagKey, new ArrayList<Integer>());
		}
		
		List<Integer> edges = tagGraph.get(tagKey);
		// FIXME -- remove check when stable
		if (edges.contains(tagVal)) throw new RuntimeException("Tag " + Integer.toString(tagKey) + 
				" already contains edge " + Integer.toString(tagVal));
		edges.add(tagVal);
	}
	
	public int getTag(Block block) {
		if (!tagMap.containsKey(block.toString()))
			throw new RuntimeException(block.toString());
		return tagMap.get(block.toString());
	}
	
	public List<Integer> getEdges(int tag) {
		return tagGraph.get(tag);
	}
	
	public void setUp(BriefBlockGraph graph) {
		this.tagMap = new HashMap<String, Integer>();
		this.tagGraph = new HashMap<Integer, List<Integer>>();
		List<Block> blocks = graph.getBlocks();
		for (Block block : blocks) {
			Iterator<Unit> uIt = block.iterator();
			//System.out.println("Block: " + block.toString());
			while (uIt.hasNext()) {
				Unit unit = uIt.next();
				//System.out.println("Unit: " + unit.toString());
				if (unit instanceof InvokeStmt) {
					InvokeStmt invokeStmt = (InvokeStmt)unit;
					InvokeExpr invokeExpr = invokeStmt.getInvokeExpr();
					SootMethod sMethod = invokeExpr.getMethod();
					if (sMethod.getSubSignature().equals(addTagSignature)) {
						addTagToMap(block, ((IntConstant)invokeExpr.getArg(0)).value);
						//System.out.println("YES:" + sMethod.getSubSignature());
					} else {
						//System.out.println("NO: " + sMethod.getSubSignature());
					}
				} 
			}
			//System.out.println("");
		}
		
		for (Block block : blocks) {
			for (Block target : graph.getSuccsOf(block))
				addTagToGraph(getTag(block), getTag(target));
		}
	}
}
