package at.util;

import java.util.*;

import at.ir.Expr;
import at.ir.Stmt;

public class Containers {
	private Containers() {}
	
	public static <T> Set<T> listToSet(List<T> list) {
		Set<T> set = new HashSet<T>();
		for (T item : list) 
			set.add(item);
		return set;
	}
	
	public static <T> void Swap(T v1, T v2) {
		T tmp = v1;
		v1 = v2;
		v2 = tmp;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Expr> List<T> cloneExpr(List<T> lis) {
		List<T> newList = new ArrayList<T>();
		for (T elem: lis) newList.add((T) elem.clone());
		return newList;
	}

	@SuppressWarnings("unchecked")
	public static <T extends Stmt> List<T> cloneStmt(List<T> lis) {
		List<T> newList = new ArrayList<T>();
		for (T elem: lis) newList.add((T) elem.clone());
		return newList;
	}
	
	public static <K, V> List<K> mapKeysToList(Map<K, V> map) {
		List<K> newList = new ArrayList<K>();
		for (Map.Entry<K, V> entry : map.entrySet()) {
			newList.add(entry.getKey());
		}
		return newList;
	}

	public static <K, V> List<V> mapValuesToList(Map<K, V> map) {
		List<V> newList = new ArrayList<V>();
		for (Map.Entry<K, V> entry : map.entrySet()) {
			newList.add(entry.getValue());
		}
		return newList;
	}
}
