package at.util;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;



public class IO {
	
	public static List<String> filesInDir(File dir, String extension) {
		List<String> files = new ArrayList<String>();
		for (File file : dir.listFiles()) {
			if (file.isDirectory())
				files.addAll(filesInDir(file, extension));
			else if (file.toString().matches(".*\\." + extension)) {
				files.add(file.toString());
			}
		}
		return files;
	}
	
	public static void cleanDirectory(File dir, String extension) throws IOException {
		for (File file : dir.listFiles()) {
			if (!file.isDirectory()) {
				if (file.toString().matches(".*\\." + extension))
					file.delete();
			} else { 
				cleanDirectory(file, extension);
			}
		}
	}

	public static void copyFiles(String inputDir, String outputDir, String extension) throws IOException {
	    Files.find(Paths.get(inputDir), 
	    		999, (p, bfa) -> bfa.isRegularFile()
	    		&& p.getFileName().toString().matches(".*\\." + extension)).forEach(
	    				(p) -> { 
	    					String[] parts = p.toString().split("/");
	    					parts[0] = outputDir;
							try {
								Files.copy(Paths.get(p.toString()),
										Paths.get(String.join("/", parts)));
							} catch (Exception e) {
								e.printStackTrace();
							}
	    					});
	}

	public static String processOutputToString(Process process) throws InterruptedException, IOException {
		process.waitFor();
		String line = "";
		StringBuilder builder = new StringBuilder();

		BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		while((line = error.readLine()) != null) {
			builder.append(line);
			builder.append(System.getProperty("line.separator"));
		}
		error.close();

		BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
		while((line = input.readLine()) != null) {
			builder.append(line);
			builder.append(System.getProperty("line.separator"));
		}
		input.close();

		return builder.toString();
	}

	public static void printProcessOutput(Process process) throws InterruptedException, IOException {
		process.waitFor();
		String line;

		BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		while((line = error.readLine()) != null) System.out.println(line);
		error.close();

		BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
		while((line=input.readLine()) != null) System.out.println(line);
		input.close();

		OutputStream outputStream = process.getOutputStream();
		PrintStream printStream = new PrintStream(outputStream);
		printStream.println();
		printStream.flush();
		printStream.close();
	}
	
	public static String arrayToString(int[] array) {
		String str = "";
		for (int i = 0; i < array.length; ++i) str += " " + array[i];
		return str;
	}

	public static <T> String arrayToString(T[] array) {
		String str = "";
		for (int i = 0; i < array.length; ++i) str += " " + array[i].toString();
		return str;
	}
	
	public static <T> String setToString(Set<T> set) {
		String str = "";
		for (T elem : set) str += " " + elem;
		return str;
	}

	public static <K, V> String mapToString(Map<K, V> map) {
		String str = "";
		for (Map.Entry<K, V> entry : map.entrySet()) str += " " + entry.getKey() + ": " + entry.getValue();
		return str;
	}

	public static <T> String listToString(List<T> list) {
		String str = "";
		for (T elem : list) str += " " + elem;
		return str;
	}

}
