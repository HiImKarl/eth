package at.util;

public class Messages {
	private Messages() {}
	public static final String couldNotCreateTest = "Sorry, couldn't create test cases";
	public static final String exceptionOccured = "Exception Occured in PUT";
	public static final String JVMExceptionMessage = "Exception in thread";

}
