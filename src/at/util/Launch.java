package at.util;

import java.io.*;

import at.Main;

public class Launch {
	public static Process runInputProgram(String path, String mainClass) throws IOException, InterruptedException {
		String cmdArgs[] = {
			"java", "-cp", path + ":" + Main.utilPath + ":" + Main.outputPath, mainClass
		};

		Process process = Runtime.getRuntime().exec(cmdArgs);
		return process;
	}
}
