package at;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;

import at.util.Containers;
import at.util.Launch;
import at.util.IO;
import at.util.Messages;
import at.util.Tags;
import at.analysis.BranchFlowAnalysis;
import at.analysis.BranchFlowTransformer;
import at.analysis.Method;
import at.dynamic.ClassBuilder;
import at.eval.ArrayExpansion;
import at.eval.CaptureInformation;
import at.eval.MethodExpansion;
import at.instrument.InstrumentTransformer;
import soot.*;
import soot.toolkits.graph.BriefBlockGraph;

public class Main {

	// FIXME name of the main class, relative to "./input/"
	public static final String mainClass = "test.Test";
	
	// Project layout
	public static final String outputPath = "./output/";
	public static final String inputPath = "./input/";
	public static final String utilPath = "./util/";
	public static final String runPath  = "./run/";
	public static final String coverageJAR = "util/coverage/Coverage.jar";
	public static final String coverageName = "coverage.Coverage";
	public static final String testerName = "run.Tests";

	// FIXME -- This must be changed to the correct JRE of the system
	public static final String rtJAR = "/Library/Java/JavaVirtualMachines/jdk1.8.0_102.jdk/Contents/Home/jre/lib/rt.jar";
	public static final String jceJAR = "/Library/Java/JavaVirtualMachines/jdk1.8.0_102.jdk/Contents/Home/jre/lib/jce.jar";

	public static String classPath = 
			rtJAR + ":" +
			jceJAR + ":" +
			coverageJAR + ":" +
			inputPath + ":" +
			runPath + ":" +
			utilPath;

	// Collections of all of the instrumented/analyzed
	public static List<BriefBlockGraph> graphs; 
	public static Map<String, Method> methodMap = new HashMap<String, Method>();
	private static int methodCount = 0;
	public static int getMethodCount() { return methodCount; }
	
	// FIXME Min/Max number of loop iterations
	// These numbers should be greater than 0
	public final static int loopIterationMaximum = 5;
	public final static int loopIterationMinimum = 1;
	
	// FIXME Max number of recursive function calls
	// This numbers should be greater than 0
	public final static int recursionLimit = 6;

	// FIXME Bounds on the input range of the test cases
	// Larger ranges may improve chances of obtaining a correct
	// solution but may make it difficult for the solver to converge
	private static final int chocoMin = -10000;
	private static final int chocoMax = 10000;
	public static final byte chocoByteMin = Byte.MIN_VALUE;
	public static final byte chocoByteMax = Byte.MAX_VALUE;
	public static final int chocoIntMin = chocoMin;
	public static final int chocoIntMax = chocoMax;
	public static final double chocoRealPrecision = 0.0001;
	public static final double chocoRealMin = chocoMin;
	public static final double chocoRealMax = chocoMax;

	public static void main(String[] args) throws IOException, InterruptedException {
		
		// Initialize containers 
		graphs = new ArrayList<BriefBlockGraph>();

		// Arguments given to Soot main, "ws" enables the Shimple (SSA IR) phase, 
		String[] baseArgs = {
				"-cp", classPath, "-pp",
				"-ws", "-src-prec", "class",
				"-output-dir", outputPath,
				"-main-class", mainClass,
				"-f", "class",
		};

		// Collect all of class files
		List<String> tmp = IO.filesInDir(new File(inputPath), "class");
		List<String> otherClasses = new ArrayList<String>();
		for (String otherClass : tmp) {
			otherClass = otherClass.substring(inputPath.length());
			otherClass = otherClass.substring(0, otherClass.length() - 6);
			String[] parts = otherClass.split("/");
			otherClass = String.join(".", parts);
			otherClasses.add(otherClass);
		}
		
		// Copy class files from input folder to output folder
		IO.cleanDirectory(new File(outputPath), "class");
		IO.copyFiles("input", "output", "class");

		// Add coverage tagging class to Soot
		Scene.v().addBasicClass(coverageName, SootClass.SIGNATURES);
		
		// Add class files to Soot arguments
		String[] sootArgs = ArrayUtils.addAll(baseArgs, otherClasses.stream().toArray(String[]::new));
		
		// Instrument and create program flow graphs
		// transformation phase
		PackManager.v().getPack("stp").add(new Transform("stp.instrument", InstrumentTransformer.v()));
		PackManager.v().getPack("stp").add(new Transform("stp.branch", BranchFlowTransformer.v()));
		
		// Call Soot
		soot.Main.main(sootArgs);
		System.out.println("");
		
		// Perform branch analysis for each method
		for (BriefBlockGraph graph : graphs) {
			if (graph == null) throw new RuntimeException();
			Tags.v().setUp(graph);
			Method method = BranchFlowAnalysis.v().analyze(graph);
			Scene.v().addBasicClass(method.getClassName(), SootClass.SIGNATURES);
			methodMap.put(method.getFullName(), method);
			++methodCount;
		}
		
		ClassBuilder.v().createTestClass();
		MethodExpansion.v().expandMethods(Containers.mapValuesToList(methodMap));
		CaptureInformation.v().arrayInformation(Containers.mapValuesToList(methodMap));
		ArrayExpansion.v().expandArrays(Containers.mapValuesToList(methodMap));
		for (Map.Entry<String, Method> entry: methodMap.entrySet()) {
			Method method = entry.getValue();
			method.solve();
			// FIXME uncomment the following to print the program paths 
			//if (method.getClassName().equals(mainClass)) System.out.println(method.toString());
			if (!method.hasSolutions())
				System.out.println("Could not find any solutions for " + method.getName());
			// Don't create tests for constructors
			else if (!method.isConstructor())
				ClassBuilder.v().CreateTestMethod(method);
		}

		System.out.println("");

		// Build the test cases and run them
		ClassBuilder.v().createTestMain();
		ClassBuilder.v().writeClass();
		Process process = Launch.runInputProgram(".", testerName);
		
		// Process output. If no test cases were ran, indicate so
		String output = IO.processOutputToString(process);
		if (output.length() == 0) {
			System.out.println("No test cases");
			return;
		}

		// Panic if an exception occurs in the JVM while executing
		if (output.contains(Messages.JVMExceptionMessage)) {
			System.out.println(Messages.couldNotCreateTest + ": " + Messages.exceptionOccured);
			return;
		}

		// Parse and collect the tags, grouped by method
		System.out.println("Output: " + output);
		String[] pathOutputs = output.split("\n");
		Map<String, Set<Integer>> methodTagMap = new HashMap<String, Set<Integer>>();
		for (String pathOutput : pathOutputs) {
			if (pathOutputs.length == 0) continue;
			String[] nameAndTags = pathOutput.split("!");
			String[] tags = nameAndTags[1].split(",");
			methodTagMap.put(nameAndTags[0], parseTags(tags));
		}
	
		System.out.println("");
		// Print the coverage for each method
		for (Map.Entry<String, Method> entry: methodMap.entrySet()) {
			Method method = entry.getValue();
			// Only instrument class under test methods
			if (!method.getClassName().equals(mainClass)) continue;
			// if the method wasn't printed, it wasn't set up for 
			// analysis in the first place
			if (!methodTagMap.containsKey(method.getFullName())) continue;
			Set<Integer> tags = methodTagMap.get(method.getFullName());
			tags.retainAll(method.getTags());
			System.out.println("Method: " + entry.getKey());
			System.out.println("Tags: " + IO.setToString(tags));
			System.out.println("Coverage:" + (double)tags.size()/method.getNumTags() * 100 + "%");
		}

		System.out.println("Finished");
	}
	
	public static Set<Integer> parseTags(String[] strTags) {
		Set<Integer> tags = new HashSet<Integer>();
		for (int i = 0; i < strTags.length; ++i) {
			int tag = Integer.parseInt(strTags[i]);
			if (!tags.contains(tag)) tags.add(tag);
		}
		return tags;
	}
}
