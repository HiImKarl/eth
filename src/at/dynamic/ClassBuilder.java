package at.dynamic;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import at.Main;
import at.analysis.Method;
import at.analysis.ProgramPath;
import at.ir.Expr;
import soot.ArrayType;
import soot.Local;
import soot.Modifier;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.Value;
import soot.VoidType;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.JasminClass;
import soot.jimple.Jimple;
import soot.jimple.JimpleBody;
import soot.jimple.ReturnVoidStmt;
import soot.util.JasminOutputStream;
import soot.util.Chain;

public class ClassBuilder {
	// Creates the class with methods that contain the test cases and
	// a main method that runs the test cases
	private static ClassBuilder instance = new ClassBuilder();
	private ClassBuilder() {}
	public static ClassBuilder v() { return instance; }

	private static final String printTagsSignature = "void printTags()";
	
	private SootClass testClass = null;
	private List<SootMethod> testMethods = null;
	
	public void createTestClass() {
		Scene.v().loadClassAndSupport("java.lang.Object");
		Scene.v().loadClassAndSupport("java.lang.System");
		
		SootClass sClass = new SootClass(Main.testerName, Modifier.PUBLIC);
		sClass.setSuperclass(Scene.v().getSootClass("java.lang.Object"));
		Scene.v().addClass(sClass);
		this.testClass = sClass;
		this.testMethods = new ArrayList<SootMethod>();
	}
	
	public void createTestMain() {
		SootMethod method = new SootMethod("main", 
				Arrays.asList(new Type[]{ArrayType.v(RefType.v("java.lang.String"), 1)}),
				VoidType.v(), Modifier.PUBLIC | Modifier.STATIC);
		testClass.addMethod(method);
		JimpleBody body = Jimple.v().newBody(method);
		method.setActiveBody(body);

		Local l0 = Jimple.v().newLocal("l0", ArrayType.v(RefType.v("java.lang.String"), 1));
		Local printRef = Jimple.v().newLocal("printStreamRef", RefType.v("java.io.Printstream"));
		body.getLocals().add(l0);
		body.getLocals().add(printRef);
		
		Chain<Unit> units = body.getUnits();
		units.add(Jimple.v().newIdentityStmt(l0, Jimple.v().newParameterRef(
				ArrayType.v(RefType.v("java.lang.String"), 1), 0))); 
		units.add(Jimple.v().newAssignStmt(printRef, Jimple.v().newStaticFieldRef(
				Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())));
		List<Value> argList = new ArrayList<Value>();
		for (SootMethod sMethod : testMethods)
			units.add(Jimple.v().newInvokeStmt(
					Jimple.v().newStaticInvokeExpr(sMethod.makeRef(), argList)));
		units.add(Jimple.v().newReturnVoidStmt());
	}

	public void CreateTestMethod(Method method) throws IOException {
		SootClass coverageClass = Scene.v().loadClassAndSupport(at.Main.coverageName);
		SootMethod clearMethod = coverageClass.getMethod("void clear()");

		String methodName = method.getClassName().replace(".", "_") + "_" + method.getName().replaceAll("[ ()]", "_");
		SootMethod sMethod = new SootMethod(methodName, Arrays.asList(), VoidType.v(), Modifier.PUBLIC | Modifier.STATIC);
		SootMethod sootMethod = method.getSootMethod();
		// FIXME -- set up tests for non-primitive/non-static methods
		if (!method.getSootMethod().isStatic() || !method.hasPrimitiveReturnType()) return;
		testMethods.add(sMethod);
		testClass.addMethod(sMethod);
		JimpleBody body = Jimple.v().newBody(sMethod);
		sMethod.setActiveBody(body);
		
		Chain<Unit> units = body.getUnits();
		Local classRef = Jimple.v().newLocal("classRef", RefType.v(method.getClassName())); 
		body.getLocals().add(classRef);
		SootMethod printlnMethod = Scene.v().getMethod("<java.io.PrintStream: void println(java.lang.String)>");
		
		units.add(Jimple.v().newInvokeStmt(
				Jimple.v().newStaticInvokeExpr(clearMethod.makeRef())));
		
		List<Value> arguments = new ArrayList<Value>();
		for (ProgramPath path : method.getPaths()) {
			if (!path.hasSolutions()) continue;
			for (Expr argExpr : Method.solutionsList(path.getSolutions())) 
				arguments.add(Expr.convertToStoot(argExpr));
			units.add(Jimple.v().newInvokeStmt(
						Jimple.v().newStaticInvokeExpr(sootMethod.makeRef(), arguments)));
			arguments.clear();
		}
		SootMethod printTags = coverageClass.getMethod(printTagsSignature);

		ReturnVoidStmt returnStmt = Jimple.v().newReturnVoidStmt();
		units.add(returnStmt);
		InvokeExpr reportExpr = Jimple.v().newStaticInvokeExpr(printTags.makeRef());
		InvokeStmt reportStmt = Jimple.v().newInvokeStmt(reportExpr);
		units.insertBefore(reportStmt, returnStmt);
	}
	
	public void writeClass() throws IOException {
		String fileName = Main.testerName.replace(".", "/") + ".class";
		OutputStream sOut = new JasminOutputStream(new FileOutputStream(fileName));
		PrintWriter writer = new PrintWriter(new OutputStreamWriter(sOut));
		JasminClass jasminClass = new JasminClass(testClass);
		jasminClass.print(writer);
		writer.flush();
		sOut.close();
	}
}
