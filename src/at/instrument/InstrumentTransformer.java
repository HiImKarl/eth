package at.instrument;

import java.util.*;

import at.analysis.Method;
import soot.*;
import soot.jimple.*;

public class InstrumentTransformer extends BodyTransformer {
	private static InstrumentTransformer instance = new InstrumentTransformer();
	public static InstrumentTransformer v() { return instance; }
	private InstrumentTransformer() {}

	private SootClass coverageClass;
	private SootMethod setMethod;
	private SootMethod addTag;
	private boolean coverageLoaded = false;
	
	private int tag = 0;
	private static final String addTagSignature = "void addTag(int)";
	
	private void loadCoverageClass() {
		if (!coverageLoaded) {
			coverageClass = Scene.v().loadClassAndSupport(at.Main.coverageName);
			setMethod = coverageClass.getMethod("void setMethod(java.lang.String)");
			addTag = coverageClass.getMethod(addTagSignature);
			coverageLoaded = true;
		}
	}
	
	private void instrument(Body body) {
		if (isInstrumented(body)) return;
		SootMethod sMethod = body.getMethod();
		PatchingChain<Unit> units = body.getUnits();
		
		// Add methods that log class information to the head of the method body
		Iterator<Unit> ssIt = units.snapshotIterator();
		while (ssIt.hasNext()) {
			// FIXME -- Figure out why invoke statements are baf at this stage?
			Unit unit = ssIt.next();
			if (!(unit instanceof Stmt)) continue;
			Stmt stmt = (Stmt)unit;
			if (!(stmt instanceof IdentityStmt)) {
				String methodName = Method.methodFullName(
						sMethod.getDeclaringClass().getName(), sMethod.getSubSignature());
				InvokeExpr methodExpr = Jimple.v().newStaticInvokeExpr(
						setMethod.makeRef(), StringConstant.v(methodName));
				InvokeStmt methodStmt = Jimple.v().newInvokeStmt(methodExpr);
				units.insertBefore(methodStmt, stmt);
				InvokeExpr tagExpr = Jimple.v().newStaticInvokeExpr(
						addTag.makeRef(), IntConstant.v(tag));
				++tag;
				Stmt tagStmt = Jimple.v().newInvokeStmt(tagExpr);
				units.insertAfter(tagStmt, methodStmt);
				break;
			} 
		}

		// Add method that prints tags out to the end of the method body
		units = body.getUnits();
		ssIt = units.snapshotIterator();
		boolean insert = false;
		while (ssIt.hasNext()) {
			// FIXME -- Figure out why invoke statements are baf at this stage?
			Unit unit = ssIt.next();
			if (!(unit instanceof Stmt)) continue;
			Stmt stmt = (Stmt)unit;
			if (insert) {
				addTag(stmt, units);
				insert = false;
			}
			if (stmt instanceof IfStmt) {
				for (UnitBox targetBox : stmt.getUnitBoxes())
					addTag(targetBox.getUnit(), units);
				insert = true;
			} else if (stmt instanceof GotoStmt) {
				UnitBox targetBox = stmt.getUnitBoxes().get(0);
				addTag(targetBox.getUnit(), units);
			}
		}
		
		// Instrument the methods that this method invokes
		units = body.getUnits();
		Iterator<Unit> uIt = units.snapshotIterator();
		while (uIt.hasNext()) {
			instrumentStmt(uIt.next());
		}
	}
	
	private void instrumentStmt(Unit stmt) {
		if (stmt instanceof InvokeStmt) {
			InvokeStmt iStmt = (InvokeStmt) stmt;
			instrumentValue(iStmt.getInvokeExpr());
		} else if (stmt instanceof AssignStmt) {
			AssignStmt assignStmt = (AssignStmt) stmt;
			instrumentValue(assignStmt.getRightOp());
		}
	}
	
	private void instrumentValue(Value value) {
		if (value instanceof InvokeExpr) {
			InvokeExpr iExpr = (InvokeExpr) value;
			if (iExpr.getMethod().hasActiveBody())
				instrument(iExpr.getMethod().getActiveBody());
		} 
	}
	
	@Override
	protected void internalTransform(Body body, String phaseName,
			Map<String, String> Options) {
		loadCoverageClass();
		// Don't do anything with initializers
		if (body.getMethod().getSubSignature().equals("void <init>()")) return;
		// Strings aren't supported yet
		if (body.getMethod().getSubSignature().equals("void main(java.lang.String[])")) return;
		instrument(body);
	}
	
	private void addTag(Unit unit, PatchingChain<Unit> units) {
		if (checkTagExists(unit)) return;
		InvokeExpr tagExpr = Jimple.v().newStaticInvokeExpr(
				addTag.makeRef(), IntConstant.v(tag));
		InvokeStmt tagStmt = Jimple.v().newInvokeStmt(tagExpr);
		units.insertBefore(tagStmt, unit);
		++tag;
	}
	
	private boolean checkTagExists(Unit stmt) {
		if (stmt instanceof InvokeStmt) {
			InvokeExpr iExpr = (InvokeExpr) ((InvokeStmt)stmt).getInvokeExpr();
			if (iExpr.getMethod().getSubSignature().equals(addTagSignature))
				return true;
		}
		return false;
	}
	
	private boolean isInstrumented(Body body) {
		PatchingChain<Unit> units = body.getUnits();
		Iterator<Unit> uIt = units.snapshotIterator();
		while (uIt.hasNext()) {
			Unit unit = uIt.next();
			if (unit instanceof InvokeStmt) {
				InvokeStmt iStmt = (InvokeStmt) unit;
				if (iStmt.getInvokeExpr() instanceof StaticInvokeExpr && 
						iStmt.getInvokeExpr().getMethod().getSignature().equals(setMethod.getSignature()))
					return true;
				return false;
			}
		}
		return false;
	}
}
