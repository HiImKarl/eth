package test;

public class Test {

  // This method will not be instrumented because
  // the tool currently does not have support for Object 
  // or array arguments. It must be present however, or
  // Soot complains.
  public static void main(String[] args) {}

  /*
  public static int method1(int x) {
    if (x == 25) return 12;
    return 34;
  }

  public static boolean simple_bool(boolean b) {
    if (b) return false;
    return true;
  }

  public static int recurse(int x) {
    if (x == 0) return 1;
    if (x == 1) return 1;
    return recurse(x - 1) + recurse2(x - 2);
  }

  public static int recurse2(int x) {
    if (x == 0) return 0;
    return x * recurse(x - 1);
  }

  public static int method2(int x, int y) {
    if (x > method1(y)) return x - y;
    return x;
  }

  public static int loop_fn(int x) {
    int y = 3;
    for (int i = 24; i < x; ++i)
      y += method1(i);
    if (y > 15) return 1;
    else return -1;
  }

  public static void ref(int i, int j) {
    int x[] = {1, 2, 3};
    int y[] = {2, 3};
    if (x[i] == x[j])
      y = x;
  }

  public static double loop(int x, int y) {
    int z = 10;
    for (int i = 0; i < 10; ++i) {
        if (i + x == 200) z += x * y;
        z *= -1;
    }
    return z;
  }

  public static boolean loop2(int x, int y) {
    int z = 10;
    for (int i = 0; i < x; i += y) {
      if (z + y > 100) z -= 10;
      else --y;
    }
    if (z + y > 100) return true;
    return false;
  }

  public static int loop3(int x, int y) {
    int z = 12;
    for (int i = x; i < y; ++i) {
      if (i == 1000 + x - y) return y;
      else z -= 2;
    }
    if (z == 8) return 1;
    return 0;
  }

  public static boolean array(int x, int y) {
    int arr[] = {1, 2, 3};
    arr[x] += 12;
    arr[y] += 13;
    if (arr[x] == arr[y]) return false;
    return true;
  }

  public static int array_loop(int x) {
    int arr1[] = {4, 2, 1};
    for (int i = 0; i < 3; ++i)
    if (arr1[x] == i) return arr1[x + 1];
    return 0;
  }

  public static int[] array_ret_fn() {
    int array[] = {4, 0, 1};
    return array;
  }

  public static int array_fn(int x, int y) {
    int arr1[] = {1, 2, 3, 9, 10}; 
    int arr2[] = array_ret_fn();
    if (arr1[x] == arr2[y]) return arr1[y] + arr2[x + 1];
    return 0;
  }

  public static boolean array_id(int x, int y) {
    int arr1[] = {1, 2, 3};
    int arr2[] = new int[12];
    if (x == y * 14) {
      arr2 = arr1;
    }
    if (arr1 == arr2) return true;
    return false;
  }

  public static int obj(int x, int y) {
    A num = new A(x, y);
    num.incr(x);
    num.decr(x);
    if (num.x == num.y) 
      return -1;
    return 0;
  }

  public static boolean obj_loop(int x, int y) {
    A num = new A (x, y);
    for (int i = 0; i < x; ++i)
      num.decr(i);
    if (num.y == num.x) return true;
    return false;
  }

  // Note that this method will not be tested because non
  // primitive return values are not yet supported
  public static A test() {
    A num = new A(1, 1);
    return num;
  }

  public static boolean obj_fn(int x, int y) {
    A num = test();
    num.incr(y);
    if (num.getX() == x) return true;
    return false;
  }

  public static boolean obj_id(int x, int y) {
    A num1 = new A(x, y);
    A num2 = new A(y, x);
    if (num1.getX() == num2.getX())
      num2 = num1;
    if (num1 == num2) return true;
    return false;
  }

  public static int nested_obj(int x, int y, int z) {
    B b = new B(x, y, z);
    if (b.z == b.a.y + b.a.x) return b.a.y;
    else return b.z;
  }

  public static boolean recursive_obj(int x, int y, int z) {
    C c = new C(x, y, z);
    if (c.x == c.d.y) return false;
    return true;
  }


  // These method won't achieve 100% coverage unless the loop depth
  // parameters are tweaked
  
  public static int loop4(int x) {
    for (int i = 0; i < x; ++i)
      if (i == 8) return 1;
    return -1;
  }

  */
  public static int recurse3(int x) {
    if (x == 0) return 1;
    return recurse3(x - 1);
  }

  public static boolean recurse4(int x) {
    if (recurse3(4) == x) return true;
    return false;
  }

}

// Simple class for testing. It's methods are not included in the 
// test suite.
class A {
  public int x;
  public int y;

  public A(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() { return x; }
  public int getY() { return y; }

  public void incr(int i) {
    this.x += i;
  }

  public void decr(int i) {
    this.y -= i;
  }
}

// Another simple class to test nested objects
class B {
  public int z;
  public A   a;

  public B(int x, int y, int z) {
    this.a = new A(x, y);
    this.z = z;
  }
}

// Recursive classes
class C {
  public int x;
  public D   d;

  public C(int x, int y, int z) {
    this.d = new D(y, z);
    this.x = x;
  }

  public C(int x, int y) {
    this.d = new D(y);
    this.x = x;
  }

  public C(int x) {
    this.d = null;
    this.x = x;
  }
}

class D {
  public int y;
  public C c;
  public D(int x, int y) {
    this.c = new C(x);
    this.y = y;
  }

  public D(int y) {
    this.c = null;
    this.y = y;
  }
}
