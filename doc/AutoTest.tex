\documentclass[]{article}
\usepackage{listings}

% Title Page
\title{Automatic Generation of Instruction Coverage Test Cases for Java Programs}
\author{Karl Chen}

\begin{document}
\maketitle

\begin{abstract}

\end{abstract}

\section*{ Introduction }
	One metric for building good program test suites is instruction coverage, the percentage of instructions (bytecode/machine code) run by a given test case, out of the total number of program instructions. It would be useful, if given an arbitrary program, to produce automatically a suite of test cases that will maximize instruction coverage. These tests could be used to find crashes or, for academic purposes, perform validation against a correct program.
	
	This paper presents a systematic approach to the aforementioned test case generation for Java programs, and includes a prototype tool to demonstrate key ideas. Importantly, all test inputs are produced deterministically by statically analyzing the program's control graph. This, however, limits the acceptable programs to those without any indeterministic elements, such as finalizers, parrellization, user input, etc.

\section*{ Methodology }
\subsection*{ Program Paths }
	
	The number of possible paths that can be taken in each method is $O(2^n)$, where $n$ is the number of conditional branch statements in the method. The set of all possible program paths is constructed by permuting over the possible conditional branch targets. In the prototype implementation, these are represented by $at.analysis.ProgramPath$.
	
\subsection*{ Conditions }

	From the conditional branches, there is a set of conditions that must be satisified for the program to follow a certain path. These are logical equations that can be broken down and simplified into linear constraints whose arguments are the parameters arguments of the method. Each condition is evaluated backwards from its point of origin, substituting variables in the condition by evaulating assignment statements in the source bytecode. All of the arguments of the condition will be composed of method parameters and constants afterwards. This process occurs in $at.eval.Simplify$.
			
	This set of constraints is solved by a linear solver to produce the parameter arguments for the test suite. In the prototype implementation, these conditions are represented by $at.ir.ComparisonExpr$ and may contain parameters, variables, references, and integral constants.
	
\subsection*{ Loops }
	Loops are represented in the program flow graph as cycles. It is impossible to determine, for any given input, how many times a loop will iterate, however, it is  possible to determine, given a specific number of iterations of a specifc loop, what inputs would cause that loop to iterate the specified number of times (if at all possible). Thus, loops are handled by unrolling them up to a certain depth, and recording each depth as a different program path. The tool allows the user to specify the maximum and minimum depths, variables $at.Main.loopIterationMaximum$ and $at.Main.LoopIterationMinimum$ in the prototype, that the loop should be unrolled to. The logic for loop unrolling is in the same class as the logic for program path generation, in $at.analysis.BranchFlowAnalysis$.
	
	For example, a simple Java for loop whose upper limit is bound by some variable $x$:
	
\begin{lstlisting}[language=Java]
	for (int i = 0; i < x; ++i) {}
\end{lstlisting}
	This is the equivalent to the following, represented in Java bytecode-psuedocode:
\begin{lstlisting}[numbers=left]
	i = 0
	if (i > x) goto 6
	incr i
	goto 1
	...  ;next instruction
\end{lstlisting}
	
	If $N$ is the depth limit, this will be unrolled into $N$ paths by constraining $x$ to equal $[0, ..., N)$. It is possible for all of the unrolled loops to be unresolvable. 
	
	For example, if $N$ is less than 10, then the following loop will have no paths where the body of the loop is executed that can be solved:
	
\begin{lstlisting}[language=Java]
	for (int i = 0; i < 11; ++i) {}
\end{lstlisting}

\subsection* { Recursion }

	Recursion is similar to looping: a cycle is created in the program flow graph. The tool will also unroll recursive function calls up to a certain user-specifiable depth. In the prototype, $at.eval.MethodExpansion$ handles expanding methods and recursive calls.

\subsection* { Arrays }

	Array equivalence can be evaluated by comparing the underlying pointers. The prototype stores a unique ID for every new array creation, which can be seen in $at.ir.ArrayVar$. Array references are simply evaluated as their underlying data type. If the index to the array reference is not a constant, then paths will be replicated $N$ times, where $N$ is the size of the array, and each path sets (and constrains) the index to a distinct number between $[0, ..., N)$. 
	
	A current limitation of the implementation is that Array sizes must be known at compile time. The approach to supporting dynamic array sizes would have to be to create multiple paths, and once again fix the array size, as a condition, to a different value in each path. Similarily to the for loop and recursion, maximal and minimal limits can be user defined for the array sizes.

\subsection* {Objects}

	Like Objects, Array equivlance is evluated by comparing underyling pointers, which is represented by a unique ID associated with every new object expression. Otherwise, objects are simply treated as collections of primitives and arrays and make no difference in regards to how the paths are reduced and evaluated. Objects are represented in the prototype by $at.ir.ObjectVar$.
	
\subsection*{ Solving Conditions }

	The number of program paths that are possible is exponential regarding the number of branch statements in the program. Thus, restricting the number of paths to analyze into a minimum viable set is crucial for efficieny. A polynomial minimum viable set approximation is used, where the next path chosen to analyze is always one that reaches the largest number of uncovered control blocks, until all of the blocks are covered. In the prototype, this algorithm is implemeneted in $at.eval.Reducer$ and $at.analysis.Method: void solve()$.
	
	Since every condition can be represented by a linear constraint,  any linear constraint solver capable of handling both integers and floating point values can be used to solve the sets of conditions. Any sets of conditions that are unsolvable are skipped and discarded. A theoretical coverage, the percentage of unique branch blocks that should be covered by the solved solutions, is reported.
	
\subsection*{ Dynamic Evaluation }
	
	The test cases are ran with the instrumented bytecode to evaluate the true number of blocks covered by the test cases, which is then reported to the user. 
	
	Because the entire test suite is run together in a single process, if a program exception occurs during execution of a single test case, the results of all test cases are scrapped and the tool fails to create a coverage report.
	
\section*{ Dependencies }
	Conversion from Java Bytecode to SSA IR, program control graph generation, and Bytecode instrumentation is done using Soot, an open source Java optimization framework\cite{Vallee-Rai:1999:SJB:781995.782008}. 
	
	The branch constraints are solved using Choco, a Java integer constraint programming library\cite{choco}, which has bridge code to Ibex, a floating point constraint solver.
	
	Several test cases that showcase the capabilities and limitations of this approach are included with the source code in $/input/test/Test.java$.
	
\section*{Improvements}

	The tool is missing various features, some of which could be implemented using the theory covered above.

\begin{enumerate}
	\item The ability to analyze  methods which take Object parameters. This involves solving for the values that the object's fields must have, and then figuring out if its possible to construct an object with those field values.
	\item The ability to analyze non-static methods. This is essentially an equivalent problem to 1.
	\item The ability to analyze methods which taken Array parameters. This requires creating multiple paths for each array size in a user-specified interval, similar to the discussion of non-static array sizes in Methodology.Arrays.
	\item The ability for users to specify iteration/recursion/etc expansion depths for specific methods that will be analyzed, instead of applying the same depth to all of the methods. Potentially, this can also be done for individual for loops, method calls, etc.
	
\end{enumerate}

	The tool does not cover all of the possible features of Java. These are some of the obvious features it is missing (no where near exhaustive), and which could potentially be implemented in the future.
	
\begin{enumerate}
	\item It currently does not support byte, char, short, long, or floats. Adding these would mostly be trivial, however a solver than can handle 64bit range is necessary to handle long values., which Choco does not support.
	\item Bitwise operators (i.e bitwise or, and, xor).
	\item Null references
	\item Polymorphism
	\item Switch statements, which can be handled the same way nested if statements or if-else statements can be.
	\item Break, continue, labeled goto statements.
	\item Exceptions.
\end{enumerate}

\bibliographystyle{plain}
\bibliography{references}

\end{document}
