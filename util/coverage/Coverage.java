package coverage;
import java.util.*;

public class Coverage {
    private static Coverage instance = new Coverage();
    public static Coverage v() { return instance; }
    private Coverage() {}

    private static Map<String, Set<Integer>> methodMap = new HashMap<String, Set<Integer>>();
    private static Set<Integer> currMethod    = null;
    private static String currMethodName = null;
    private static boolean setMethodFlag = false;

    public static Set<Integer> getCurrMethod() {
        return currMethod;
    }

    public static void clear() {
	methodMap.clear();
        setMethodFlag = true;
    }

    public static void addTag(int tag) {
		if (!currMethod.contains(tag))
			currMethod.add(tag);
    }

    public static void printTags() {
		System.out.printf(currMethodName + "!");
		for (Integer tag : currMethod)
			System.out.printf(tag + ",");
		System.out.println();
    }

    public static void setMethod(String methodName) {
        if (setMethodFlag == false) return;
        setMethodFlag = false;
        Set<Integer> method = methodMap.get(methodName);
        if (method == null) {
            method = new HashSet<Integer>();
            methodMap.put(methodName, method);
        }
        currMethod = method;
	currMethodName = methodName;
    }
}
